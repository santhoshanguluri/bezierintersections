//#include "kisbooleanoperations.h"

//#include <kisintersectionfinder.h>
//#include <painterpath.h>
//#include <painterpath_p.h>
//#include <pathclipper.h>
//#include <QPainterPath>

//template <typename InputIterator>
//InputIterator qFuzzyFind(InputIterator first, InputIterator last, qreal val)
//{
//    while (first != last && !QT_PREPEND_NAMESPACE(qFuzzyCompare)(qreal(*first), qreal(val)))
//        ++first;
//    return first;
//}



//KEdge::KEdge() {

//}

//KEdge::KEdge( BuildingBlock ele) : element(ele), flag(0) {

//    if (element.isCurveTo()) {

//        type = curve;

//        top = element.getCurveTo().boundingBox().top();
//        bottom = element.getCurveTo().boundingBox().bottom();
//        left = element.getCurveTo().boundingBox().left();
//        right = element.getCurveTo().boundingBox().right();
//    }

//    else {

//        type = line;

//        top = qMax(element.getLineTo().getQLine().y1(), element.getLineTo().getQLine().y2());
//        bottom = qMin(element.getLineTo().getQLine().y1(), element.getLineTo().getQLine().y2());
//        left = qMin(element.getLineTo().getQLine().x1(), element.getLineTo().getQLine().x2());
//        right = qMax(element.getLineTo().getQLine().x1(), element.getLineTo().getQLine().x2());
//    }
//}

//KisBooleanOperations::KisBooleanOperations()
//{

//}

//KisBooleanOperations::KisBooleanOperations( KisIntersectionFinder kif, Operation operation ) : vertices(kif.getRegularVertices()) {

//    op = operation;
//    Q_FOREACH(BuildingBlock bb, kif.getNetShape()) {

//        segments << bb;
//    }

//}



//bool fuzzyCompare(qreal a, qreal b)
//{
//    return qFuzzyCompare(a, b);
//}

//bool bool_op(bool a, bool b, KisBooleanOperations::Operation op)
//{
//    switch (op) {
//    case KisBooleanOperations::BoolAnd:
//        return a && b;
//    case KisBooleanOperations::BoolOr:
//    case KisBooleanOperations::Simplify:
//        return a || b;
//    case KisBooleanOperations::BoolSub:
//        return a && !b;
//    default:
//        Q_ASSERT(false);
//        return false;
//    }
//}

////KisBooleanOperations::TraversalStatus KisBooleanOperations::next(const KisBooleanOperations::TraversalStatus &status) const
////{
////    const KEdge *sp = &segments[status.edge];
////    Q_ASSERT(sp);

////    TraversalStatus result;
////    result.edge = sp->next(status.traversal, status.direction);
////    result.traversal = status.traversal;
////    result.direction = status.direction;

////    const KEdge *rp = &segments[result.edge];
////    Q_ASSERT(rp);

////    if (sp->vertex(status.direction) == rp->vertex(status.direction))
////        result.flip();

////    return result;
////}

//static void traverse(KisBooleanOperations &list, int edge, KEdge::Traversal traversal)
//{
//    KisBooleanOperations::TraversalStatus status;
//    status.edge = edge;
//    status.traversal = traversal;
//    status.direction = KEdge::Forward;

//    do {
//        int flag = status.traversal == KEdge::LeftTraversal ? 1 : 2;

//        KEdge *ep = &list.segments[status.edge];

//        ep->flag |= (flag | (flag << 4));

//#ifdef QDEBUG_CLIPPER
//        qDebug() << "traverse: adding edge " << status.edge << ", mask:" << (flag << 4) <<ep->flag;
//#endif

//        status = list.next(status);
//    } while (status.edge != edge);
//}

//static void clear(KisBooleanOperations& list, int edge, KEdge::Traversal traversal)
//{
//    KisBooleanOperations::TraversalStatus status;
//    status.edge = edge;
//    status.traversal = traversal;
//    status.direction = KEdge::Forward;

//    do {
//        if (status.traversal == KEdge::LeftTraversal)
//             list.segments[edge].flag |= 1;
//        else
//             list.segments[edge].flag |= 2;

//        status = list.next(status);
//    } while (status.edge != edge);
//}

//QVector<QCrossingEdge> KisBooleanOperations::findCrossings(qreal y)
//{
//    QVector<QCrossingEdge> crossings;

//    for (int i = 0; i < segments.size(); ++i) {
//        const KEdge *edge = &segments[i];
//        QPointF a = vertices.at( i - 1 );
//        QPointF b = vertices.at( i );

//        if ((a.y() < y && b.y() > y) || (a.y() > y && b.y() < y)) {
//            const qreal intersection = a.x() + (b.x() - a.x()) * (y - a.y()) / (b.y() - a.y());
//            const QCrossingEdge edge = { i, intersection };
//            crossings << edge;
//        }
//    }
//    return crossings;
//}

//bool KisBooleanOperations::handleCrossingEdges( qreal y, ClipperMode mode)
//{
//    QVector<QCrossingEdge> crossings = findCrossings( y);

//    Q_ASSERT(!crossings.isEmpty());
//    std::sort(crossings.begin(), crossings.end());

//    int windingA = 0;
//    int windingB = 0;

//    int windingD = 0;

//#ifdef QDEBUG_CLIPPER
//    qDebug() << "crossings:" << crossings.size();
//#endif
//    for (int i = 0; i < crossings.size() - 1; ++i) {
//        int ei = crossings.at(i).edge;
//        const KEdge *edge = &segments.at(ei);

//        windingA += edge->windingA;
//        windingB += edge->windingB;

//        const bool hasLeft = (edge->flag >> 4) & 1;
//        const bool hasRight = (edge->flag >> 4) & 2;

//        windingD += hasLeft ^ hasRight;

//        const bool inA = (windingA & aMask) != 0;
//        const bool inB = (windingB & bMask) != 0;
//        const bool inD = (windingD & 0x1) != 0;

//        const bool inside = bool_op(inA, inB, op);
//        const bool add = inD ^ inside;

//#ifdef QDEBUG_CLIPPER
//        printf("y %f, x %f, inA: %d, inB: %d, inD: %d, inside: %d, flag: %x, bezier: %p, edge: %d\n", y, crossings.at(i).x, inA, inB, inD, inside, edge->flag, edge->bezier, ei);
//#endif

//        if (add) {
//            if (mode == CheckMode)
//                return true;

//            qreal y0 = edge->top;
//            qreal y1 = edge->bottom;

////            if (y0 < y1) {
////                if (!(edge->flag & 1))
////                    traverse(list, ei, KEdge::LeftTraversal);

////                if (!(edge->flag & 2))
////                    clear(list, ei, KEdge::RightTraversal);
////            }

////            else {
//                if (!(edge->flag & 1))
//                    clear(*this, ei, KEdge::LeftTraversal);

//                if (!(edge->flag & 2))
//                    traverse(*this, ei, KEdge::RightTraversal);
////            }

//            ++windingD;
//        } else {
//            if (!(edge->flag & 1))
//                clear(*this, ei, KEdge::LeftTraversal);

//            if (!(edge->flag & 2))
//                clear(*this, ei, KEdge::RightTraversal);
//        }
//    }

//    return false;
//}


//QPainterPath KisBooleanOperations::clip( KisIntersectionFinder kif, ClipperMode mode ) {

//    //QVector<QPointF> list = kif.getRegularVertices();

//    QVector<qreal> y_coords;
//    y_coords.reserve(vertices.size());
//    for (int i = 0; i < vertices.size(); ++i)
//    {
//        y_coords << vertices.at(i).y();
//    }

//    std::sort(y_coords.begin(), y_coords.end());
//    y_coords.erase(std::unique(y_coords.begin(), y_coords.end(), fuzzyCompare), y_coords.end());

//#ifdef QDEBUG_CLIPPER
//    printf("sorted y coords:\n");
//    for (int i = 0; i < y_coords.size(); ++i) {
//        printf("%.9f\n", y_coords.at(i));
//    }
//#endif

//    bool found;

//    do {
//        found = false;
//        int index = 0;
//        qreal maxHeight = 0;
//        for (int i = 0; i < segments.size(); ++i) {
//            KEdge edge = segments.at(i);

//            // have both sides of this edge already been handled?
////            if ((edge->flag & 0x3) == 0x3)
////            {
////                continue;
////            }

////            QPathVertex *a = list.vertex(edge->first);
////            QPathVertex *b = list.vertex(edge->second);


//            if (qFuzzyCompare(edge.top, edge.bottom))
//            {
//                continue;
//            }

//            found = true;

//            qreal height = qAbs(edge.top - edge.bottom);
//            if (height > maxHeight) {
//                index = i;
//                maxHeight = height;
//            }
//        }

//        if (found) {
//            KEdge *edge = &segments[index];

////            QPathVertex *a = list.vertex(edge->first);
////            QPathVertex *b = list.vertex(edge->second);

//            // FIXME: this can be optimized by using binary search
//            const int first = qFuzzyFind(y_coords.cbegin(), y_coords.cend(), edge->bottom) - y_coords.cbegin();
//            const int last = qFuzzyFind(y_coords.cbegin() + first, y_coords.cend(),edge->top) - y_coords.cbegin();

//            Q_ASSERT(first < y_coords.size() - 1);
//            Q_ASSERT(last < y_coords.size());

//            qreal biggestGap = y_coords.at(first + 1) - y_coords.at(first);
//            int bestIdx = first;
//            for (int i = first + 1; i < last; ++i) {
//                qreal gap = y_coords.at(i + 1) - y_coords.at(i);

//                if (gap > biggestGap) {
//                    bestIdx = i;
//                    biggestGap = gap;
//                }
//            }
//            const qreal bestY = 0.5 * (y_coords.at(bestIdx) + y_coords.at(bestIdx + 1));

//#ifdef QDEBUG_CLIPPER
//            printf("y: %.9f, gap: %.9f\n", bestY, biggestGap);
//#endif

//            if (handleCrossingEdges( bestY, mode) && mode == CheckMode)
//            {
//                //return true;
//            }

//            edge->flag |= 0x3;
//        }
//    } while (found);

//    if (mode == ClipMode)
//    {
//        //list.simplify();
//    }

//    //return false;
//}




#include "kisbooleanoperations.h"
#include "kisintersectionfinder.h"
//#include "painterpath.cpp"
//#include "painterpath.h"
//#include "painterpath_p.h"
#include "kispathclipper.h"
//#include <QPainterPath>

KisBooleanOperations::KisBooleanOperations(){

}
KisBooleanOperations::~KisBooleanOperations(){

}

//QPainterPath KisBooleanOperations::toQPainterPath(const QPainterPath &path) {

//    QPainterPath res;

//    for(int i = 0; i < path.elementCount(); i++) {

//        QPainterPath::Element element = path.elementAt(i);

//        if (element.isMoveTo()) {
//            res.moveTo(element);
//        }
//        else if ( element.isLineTo()) {
//            res.lineTo(element);
//        }
//        else if (element.isCurveTo()) {
//            res.cubicTo(element, path.elementAt(i + 1), path.elementAt( i + 2));
//            i += 2;
//        }

//    }
//    return res;
//}

QPainterPath KisBooleanOperations::uniteAndAdd( QPainterPath &sub, QPainterPath &clip) {

    if (sub.isEmpty()) {
        return QPainterPath();
    }
    if (clip.isEmpty()) {
        return sub;
    }

//    QPainterPath convertedSub = toQPainterPath(sub);
//    QPainterPath convertedClip = toQPainterPath(clip);

    KisIntersectionFinder KIF(sub, clip);
    KIF.processShapes();

    QPainterPath splittedSub = KIF.subjectShapeToKisPath();
    QPainterPath splittedClip = KIF.clipShapeToKisPath();

    KisPathClipper clipper(splittedSub, splittedClip);
    QPainterPath res = clipper.clip(KisPathClipper::BoolOr);

    return (res);

}
QPainterPath KisBooleanOperations::intersectAndAdd(const QPainterPath &sub, const QPainterPath &clip) {

    if (sub.isEmpty()) {
        return QPainterPath();
    }
    if (clip.isEmpty()) {
        return sub;
    }

//    QPainterPath convertedSub = toQPainterPath(sub);
//    QPainterPath convertedClip = toQPainterPath(clip);

    KisIntersectionFinder KIF(sub, clip);
    KIF.processShapes();

    QPainterPath splittedSub = KIF.subjectShapeToKisPath();
    QPainterPath splittedClip = KIF.clipShapeToKisPath();

    KisPathClipper clipper(splittedSub, splittedClip);
    QPainterPath res = clipper.clip(KisPathClipper::BoolAnd);

    return (res);

}
QPainterPath KisBooleanOperations::subtractAndAdd(const QPainterPath &sub, const QPainterPath &clip) {

    if (sub.isEmpty()) {
        return QPainterPath();
    }
    if (clip.isEmpty()) {
        return sub;
    }

//    QPainterPath convertedSub = toQPainterPath(sub);
//    QPainterPath convertedClip = toQPainterPath(clip);

    KisIntersectionFinder KIF(sub, clip);
    KIF.processShapes();

    QPainterPath splittedSub = KIF.subjectShapeToKisPath();
    QPainterPath splittedClip = KIF.clipShapeToKisPath();

    KisPathClipper clipper(splittedSub, splittedClip);
    QPainterPath res = clipper.clip(KisPathClipper::BoolSub);

    return (res);

}



