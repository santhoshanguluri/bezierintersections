/*
 *  SPDX-FileCopyrightText: 2021 Tanmay Chavan <earendil01tc@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

//#include <qbitmap.h>
//#include <qdebug.h>
//#include <qiodevice.h>
//#include <qlist.h>
//#include <qmatrix.h>
//#include <qpen.h>
//#include <qpolygon.h>
//#include <qtextlayout.h>
//#include <qvarlengtharray.h>
//#include <qmath.h>


//#include <painterpath.h>
//#include <painterpath_p.h>
//#include <kispathclipper.h>
//#include <bezier.h>
//#include <databuffer.h>
//#include <limits.h>
//#include <QPainterPath>

//#if 0
//#include <performance.h>
//#else
//#define PM_INIT
//#define PM_MEASURE(x)
//#define PM_DISPLAY
//#endif

//QT_BEGIN_NAMESPACE

//// added by me


//quint16 b16;
//#define QT_PATH_KAPPA 0.5522847498

//bool isInf() { return (b16 & 0x7fff) == 0x7c00; }
//bool isNaN()  { return (b16 & 0x7fff) > 0x7c00; }
//bool isFinite() { return (b16 & 0x7fff) < 0x7c00; }

//bool isnan(float f) { return std::isnan(f); }
//Q_DECL_CONST_FUNCTION static inline bool qt_is_nan(float f)
//{
//    return std::isnan(f);
//}

//qreal qt_t_for_arc_angle(qreal angle)
//{
//    if (qFuzzyIsNull(angle))
//        return 0;

//    if (qFuzzyCompare(angle, qreal(90)))
//        return 1;

//    qreal radians = qDegreesToRadians(angle);
//    qreal cosAngle = qCos(radians);
//    qreal sinAngle = qSin(radians);

//    // initial guess
//    qreal tc = angle / 90;
//    // do some iterations of newton's method to approximate cosAngle
//    // finds the zero of the function b.pointAt(tc).x() - cosAngle
//    tc -= ((((2-3*QT_PATH_KAPPA) * tc + 3*(QT_PATH_KAPPA-1)) * tc) * tc + 1 - cosAngle) // value
//         / (((6-9*QT_PATH_KAPPA) * tc + 6*(QT_PATH_KAPPA-1)) * tc); // derivative
//    tc -= ((((2-3*QT_PATH_KAPPA) * tc + 3*(QT_PATH_KAPPA-1)) * tc) * tc + 1 - cosAngle) // value
//         / (((6-9*QT_PATH_KAPPA) * tc + 6*(QT_PATH_KAPPA-1)) * tc); // derivative

//    // initial guess
//    qreal ts = tc;
//    // do some iterations of newton's method to approximate sinAngle
//    // finds the zero of the function b.pointAt(tc).y() - sinAngle
//    ts -= ((((3*QT_PATH_KAPPA-2) * ts -  6*QT_PATH_KAPPA + 3) * ts + 3*QT_PATH_KAPPA) * ts - sinAngle)
//         / (((9*QT_PATH_KAPPA-6) * ts + 12*QT_PATH_KAPPA - 6) * ts + 3*QT_PATH_KAPPA);
//    ts -= ((((3*QT_PATH_KAPPA-2) * ts -  6*QT_PATH_KAPPA + 3) * ts + 3*QT_PATH_KAPPA) * ts - sinAngle)
//         / (((9*QT_PATH_KAPPA-6) * ts + 12*QT_PATH_KAPPA - 6) * ts + 3*QT_PATH_KAPPA);

//    // use the average of the t that best approximates cosAngle
//    // and the t that best approximates sinAngle
//    qreal t = 0.5 * (tc + ts);

//#if 0
//    printf("angle: %f, t: %f\n", angle, t);
//    qreal a, b, c, d;
//    bezierCoefficients(t, a, b, c, d);
//    printf("cosAngle: %.10f, value: %.10f\n", cosAngle, a + b + c * QT_PATH_KAPPA);
//    printf("sinAngle: %.10f, value: %.10f\n", sinAngle, b * QT_PATH_KAPPA + c + d);
//#endif

//    return t;
//}

//QPointF qt_curves_for_arc(const QRectF &rect, qreal startAngle, qreal sweepLength,
//                       QPointF *curves, int *point_count)
//{
//    Q_ASSERT(point_count);
//    Q_ASSERT(curves);

//    *point_count = 0;
//    if (qt_is_nan(rect.x()) || qt_is_nan(rect.y()) || qt_is_nan(rect.width()) || qt_is_nan(rect.height())
//        || qt_is_nan(startAngle) || qt_is_nan(sweepLength)) {
//        qWarning("MyPainterPath::arcTo: Adding arc where a parameter is NaN, results are undefined");
//        return QPointF();
//    }

//    if (rect.isNull()) {
//        return QPointF();
//    }

//    qreal x = rect.x();
//    qreal y = rect.y();

//    qreal w = rect.width();
//    qreal w2 = rect.width() / 2;
//    qreal w2k = w2 * QT_PATH_KAPPA;

//    qreal h = rect.height();
//    qreal h2 = rect.height() / 2;
//    qreal h2k = h2 * QT_PATH_KAPPA;

//    QPointF points[16] =
//    {
//        // start point
//        QPointF(x + w, y + h2),

//        // 0 -> 270 degrees
//        QPointF(x + w, y + h2 + h2k),
//        QPointF(x + w2 + w2k, y + h),
//        QPointF(x + w2, y + h),

//        // 270 -> 180 degrees
//        QPointF(x + w2 - w2k, y + h),
//        QPointF(x, y + h2 + h2k),
//        QPointF(x, y + h2),

//        // 180 -> 90 degrees
//        QPointF(x, y + h2 - h2k),
//        QPointF(x + w2 - w2k, y),
//        QPointF(x + w2, y),

//        // 90 -> 0 degrees
//        QPointF(x + w2 + w2k, y),
//        QPointF(x + w, y + h2 - h2k),
//        QPointF(x + w, y + h2)
//    };

//    if (sweepLength > 360) sweepLength = 360;
//    else if (sweepLength < -360) sweepLength = -360;

//    // Special case fast paths
//    if (startAngle == 0.0) {
//        if (sweepLength == 360.0) {
//            for (int i = 11; i >= 0; --i)
//                curves[(*point_count)++] = points[i];
//            return points[12];
//        } else if (sweepLength == -360.0) {
//            for (int i = 1; i <= 12; ++i)
//                curves[(*point_count)++] = points[i];
//            return points[0];
//        }
//    }

//    int startSegment = int(qFloor(startAngle / 90));
//    int endSegment = int(qFloor((startAngle + sweepLength) / 90));

//    qreal startT = (startAngle - startSegment * 90) / 90;
//    qreal endT = (startAngle + sweepLength - endSegment * 90) / 90;

//    int delta = sweepLength > 0 ? 1 : -1;
//    if (delta < 0) {
//        startT = 1 - startT;
//        endT = 1 - endT;
//    }

//    // avoid empty start segment
//    if (qFuzzyIsNull(startT - qreal(1))) {
//        startT = 0;
//        startSegment += delta;
//    }

//    // avoid empty end segment
//    if (qFuzzyIsNull(endT)) {
//        endT = 1;
//        endSegment -= delta;
//    }

//    startT = qt_t_for_arc_angle(startT * 90);
//    endT = qt_t_for_arc_angle(endT * 90);

//    const bool splitAtStart = !qFuzzyIsNull(startT);
//    const bool splitAtEnd = !qFuzzyIsNull(endT - qreal(1));

//    const int end = endSegment + delta;

//    // empty arc?
//    if (startSegment == end) {
//        const int quadrant = 3 - ((startSegment % 4) + 4) % 4;
//        const int j = 3 * quadrant;
//        return delta > 0 ? points[j + 3] : points[j];
//    }

//    QPointF startPoint, endPoint;
//    qt_find_ellipse_coords(rect, startAngle, sweepLength, &startPoint, &endPoint);

//    for (int i = startSegment; i != end; i += delta) {
//        const int quadrant = 3 - ((i % 4) + 4) % 4;
//        const int j = 3 * quadrant;

//        QBezier b;
//        if (delta > 0)
//            b = QBezier::fromPoints(points[j + 3], points[j + 2], points[j + 1], points[j]);
//        else
//            b = QBezier::fromPoints(points[j], points[j + 1], points[j + 2], points[j + 3]);

//        // empty arc?
//        if (startSegment == endSegment && qFuzzyCompare(startT, endT))
//            return startPoint;

//        if (i == startSegment) {
//            if (i == endSegment && splitAtEnd)
//                b = b.bezierOnInterval(startT, endT);
//            else if (splitAtStart)
//                b = b.bezierOnInterval(startT, 1);
//        } else if (i == endSegment && splitAtEnd) {
//            b = b.bezierOnInterval(0, endT);
//        }

//        // push control points
//        curves[(*point_count)++] = b.pt2();
//        curves[(*point_count)++] = b.pt3();
//        curves[(*point_count)++] = b.pt4();
//    }

//    Q_ASSERT(*point_count > 0);
//    curves[*(point_count)-1] = endPoint;

//    return startPoint;
//}

//// added by me fin.

//static inline bool isValidCoord(qreal c)
//{
//    if (sizeof(qreal) >= sizeof(double))
//        return qIsFinite(c) && fabs(c) < 1e128;
//    else
//        return qIsFinite(c) && fabsf(float(c)) < 1e16f;
//}

//static bool hasValidCoords(QPointF p)
//{
//    return isValidCoord(p.x()) && isValidCoord(p.y());
//}

//static bool hasValidCoords(QRectF r)
//{
//    return isValidCoord(r.x()) && isValidCoord(r.y()) && isValidCoord(r.width()) && isValidCoord(r.height());
//}

//struct MyPainterPathPrivateDeleter
//{
//    static inline void cleanup(MyPainterPathPrivate *d)
//    {
//        // note - we must downcast to MyPainterPathData since MyPainterPathPrivate
//        // has a non-virtual destructor!
//        if (d && !d->ref.deref())
//            delete static_cast<MyPainterPathData *>(d);
//    }
//};

//// This value is used to determine the length of control point vectors
//// when approximating arc segments as curves. The factor is multiplied
//// with the radius of the circle.

//// #define QPP_DEBUG
//// #define QPP_STROKE_DEBUG
////#define QPP_FILLPOLYGONS_DEBUG

//MyPainterPath qt_stroke_dash(const MyPainterPath &path, qreal *dashes, int dashCount);

//void qt_find_ellipse_coords(const QRectF &r, qreal angle, qreal length,
//                            QPointF* startPoint, QPointF *endPoint)
//{
//    if (r.isNull()) {
//        if (startPoint)
//            *startPoint = QPointF();
//        if (endPoint)
//            *endPoint = QPointF();
//        return;
//    }

//    qreal w2 = r.width() / 2;
//    qreal h2 = r.height() / 2;

//    qreal angles[2] = { angle, angle + length };
//    QPointF *points[2] = { startPoint, endPoint };

//    for (int i = 0; i < 2; ++i) {
//        if (!points[i])
//            continue;

//        qreal theta = angles[i] - 360 * qFloor(angles[i] / 360);
//        qreal t = theta / 90;
//        // truncate
//        int quadrant = int(t);
//        t -= quadrant;

//        t = qt_t_for_arc_angle(90 * t);

//        // swap x and y?
//        if (quadrant & 1)
//            t = 1 - t;

//        qreal a, b, c, d;
//        QBezier::coefficients(t, a, b, c, d);
//        QPointF p(a + b + c*QT_PATH_KAPPA, d + c + b*QT_PATH_KAPPA);

//        // left quadrants
//        if (quadrant == 1 || quadrant == 2)
//            p.rx() = -p.x();

//        // top quadrants
//        if (quadrant == 0 || quadrant == 1)
//            p.ry() = -p.y();

//        *points[i] = r.center() + QPointF(w2 * p.x(), h2 * p.y());
//    }
//}

//#ifdef QPP_DEBUG
//static void qt_debug_path(const MyPainterPath &path)
//{
//    const char *names[] = {
//        "MoveTo     ",
//        "LineTo     ",
//        "CurveTo    ",
//        "CurveToData"
//    };

//    printf("\nMyPainterPath: elementCount=%d\n", path.elementCount());
//    for (int i=0; i<path.elementCount(); ++i) {
//        const MyPainterPath::Element &e = path.elementAt(i);
//        Q_ASSERT(e.type >= 0 && e.type <= MyPainterPath::CurveToDataElement);
//        printf(" - %3d:: %s, (%.2f, %.2f)\n", i, names[e.type], e.x, e.y);
//    }
//}
//#endif

///*!
//    \class MyPainterPath
//    \ingroup painting
//    \ingroup shared
//    \inmodule QtGui

//    \brief The MyPainterPath class provides a container for painting operations,
//    enabling graphical shapes to be constructed and reused.

//    A painter path is an object composed of a number of graphical
//    building blocks, such as rectangles, ellipses, lines, and curves.
//    Building blocks can be joined in closed subpaths, for example as a
//    rectangle or an ellipse. A closed path has coinciding start and
//    end points. Or they can exist independently as unclosed subpaths,
//    such as lines and curves.

//    A MyPainterPath object can be used for filling, outlining, and
//    clipping. To generate fillable outlines for a given painter path,
//    use the MyPainterPathStroker class.  The main advantage of painter
//    paths over normal drawing operations is that complex shapes only
//    need to be created once; then they can be drawn many times using
//    only calls to the QPainter::drawPath() function.

//    MyPainterPath provides a collection of functions that can be used
//    to obtain information about the path and its elements. In addition
//    it is possible to reverse the order of the elements using the
//    toReversed() function. There are also several functions to convert
//    this painter path object into a polygon representation.

//    \tableofcontents

//    \section1 Composing a MyPainterPath

//    A MyPainterPath object can be constructed as an empty path, with a
//    given start point, or as a copy of another MyPainterPath object.
//    Once created, lines and curves can be added to the path using the
//    lineTo(), arcTo(), cubicTo() and quadTo() functions. The lines and
//    curves stretch from the currentPosition() to the position passed
//    as argument.

//    The currentPosition() of the MyPainterPath object is always the end
//    position of the last subpath that was added (or the initial start
//    point). Use the moveTo() function to move the currentPosition()
//    without adding a component. The moveTo() function implicitly
//    starts a new subpath, and closes the previous one.  Another way of
//    starting a new subpath is to call the closeSubpath() function
//    which closes the current path by adding a line from the
//    currentPosition() back to the path's start position. Note that the
//    new path will have (0, 0) as its initial currentPosition().

//    MyPainterPath class also provides several convenience functions to
//    add closed subpaths to a painter path: addEllipse(), addPath(),
//    addRect(), addRegion() and addText(). The addPolygon() function
//    adds an \e unclosed subpath. In fact, these functions are all
//    collections of moveTo(), lineTo() and cubicTo() operations.

//    In addition, a path can be added to the current path using the
//    connectPath() function. But note that this function will connect
//    the last element of the current path to the first element of given
//    one by adding a line.

//    Below is a code snippet that shows how a MyPainterPath object can
//    be used:

//    \table 70%
//    \row
//    \li \inlineimage MyPainterPath-construction.png
//    \li
//    \snippet code/src_gui_painting_MyPainterPath.cpp 0
//    \endtable

//    The painter path is initially empty when constructed. We first add
//    a rectangle, which is a closed subpath. Then we add two bezier
//    curves which together form a closed subpath even though they are
//    not closed individually. Finally we draw the entire path. The path
//    is filled using the default fill rule, Qt::OddEvenFill. Qt
//    provides two methods for filling paths:

//    \table
//    \header
//    \li Qt::OddEvenFill
//    \li Qt::WindingFill
//    \row
//    \li \inlineimage qt-fillrule-oddeven.png
//    \li \inlineimage qt-fillrule-winding.png
//    \endtable

//    See the Qt::FillRule documentation for the definition of the
//    rules. A painter path's currently set fill rule can be retrieved
//    using the fillRule() function, and altered using the setFillRule()
//    function.

//    \section1 MyPainterPath Information

//    The MyPainterPath class provides a collection of functions that
//    returns information about the path and its elements.

//    The currentPosition() function returns the end point of the last
//    subpath that was added (or the initial start point). The
//    elementAt() function can be used to retrieve the various subpath
//    elements, the \e number of elements can be retrieved using the
//    elementCount() function, and the isEmpty() function tells whether
//    this MyPainterPath object contains any elements at all.

//    The controlPointRect() function returns the rectangle containing
//    all the points and control points in this path. This function is
//    significantly faster to compute than the exact boundingRect()
//    which returns the bounding rectangle of this painter path with
//    floating point precision.

//    Finally, MyPainterPath provides the contains() function which can
//    be used to determine whether a given point or rectangle is inside
//    the path, and the intersects() function which determines if any of
//    the points inside a given rectangle also are inside this path.

//    \section1 MyPainterPath Conversion

//    For compatibility reasons, it might be required to simplify the
//    representation of a painter path: MyPainterPath provides the
//    toFillPolygon(), toFillPolygons() and toSubpathPolygons()
//    functions which convert the painter path into a polygon. The
//    toFillPolygon() returns the painter path as one single polygon,
//    while the two latter functions return a list of polygons.

//    The toFillPolygons() and toSubpathPolygons() functions are
//    provided because it is usually faster to draw several small
//    polygons than to draw one large polygon, even though the total
//    number of points drawn is the same. The difference between the two
//    is the \e number of polygons they return: The toSubpathPolygons()
//    creates one polygon for each subpath regardless of intersecting
//    subpaths (i.e. overlapping bounding rectangles), while the
//    toFillPolygons() functions creates only one polygon for
//    overlapping subpaths.

//    The toFillPolygon() and toFillPolygons() functions first convert
//    all the subpaths to polygons, then uses a rewinding technique to
//    make sure that overlapping subpaths can be filled using the
//    correct fill rule. Note that rewinding inserts additional lines in
//    the polygon so the outline of the fill polygon does not match the
//    outline of the path.

//    \section1 Examples

//    Qt provides the \l {painting/painterpaths}{Painter Paths Example}
//    and the \l {painting/deform}{Vector Deformation example} which are
//    located in Qt's example directory.

//    The \l {painting/painterpaths}{Painter Paths Example} shows how
//    painter paths can be used to build complex shapes for rendering
//    and lets the user experiment with the filling and stroking.  The
//    \l {painting/deform}{Vector Deformation Example} shows how to use
//    MyPainterPath to draw text.

//    \table
//    \header
//    \li \l {painting/painterpaths}{Painter Paths Example}
//    \li \l {painting/deform}{Vector Deformation Example}
//    \row
//    \li \inlineimage MyPainterPath-example.png
//    \li \inlineimage MyPainterPath-demo.png
//    \endtable

//    \sa MyPainterPathStroker, QPainter, QRegion, {Painter Paths Example}
//*/

///*!
//    \enum MyPainterPath::ElementType

//    This enum describes the types of elements used to connect vertices
//    in subpaths.

//    Note that elements added as closed subpaths using the
//    addEllipse(), addPath(), addPolygon(), addRect(), addRegion() and
//    addText() convenience functions, is actually added to the path as
//    a collection of separate elements using the moveTo(), lineTo() and
//    cubicTo() functions.

//    \value MoveToElement          A new subpath. See also moveTo().
//    \value LineToElement            A line. See also lineTo().
//    \value CurveToElement         A curve. See also cubicTo() and quadTo().
//    \value CurveToDataElement  The extra data required to describe a curve in
//                                               a CurveToElement element.

//    \sa elementAt(), elementCount()
//*/

///*!
//    \class MyPainterPath::Element
//    \inmodule QtGui

//    \brief The MyPainterPath::Element class specifies the position and
//    type of a subpath.

//    Once a MyPainterPath object is constructed, subpaths like lines and
//    curves can be added to the path (creating
//    MyPainterPath::LineToElement and MyPainterPath::CurveToElement
//    components).

//    The lines and curves stretch from the currentPosition() to the
//    position passed as argument. The currentPosition() of the
//    MyPainterPath object is always the end position of the last subpath
//    that was added (or the initial start point). The moveTo() function
//    can be used to move the currentPosition() without adding a line or
//    curve, creating a MyPainterPath::MoveToElement component.

//    \sa MyPainterPath
//*/

///*!
//    \variable MyPainterPath::Element::x
//    \brief the x coordinate of the element's position.

//    \sa {operator QPointF()}
//*/

///*!
//    \variable MyPainterPath::Element::y
//    \brief the y coordinate of the element's position.

//    \sa {operator QPointF()}
//*/

///*!
//    \variable MyPainterPath::Element::type
//    \brief the type of element

//    \sa isCurveTo(), isLineTo(), isMoveTo()
//*/

///*!
//    \fn bool MyPainterPath::Element::operator==(const Element &other) const
//    \since 4.2

//    Returns \c true if this element is equal to \a other;
//    otherwise returns \c false.

//    \sa operator!=()
//*/

///*!
//    \fn bool MyPainterPath::Element::operator!=(const Element &other) const
//    \since 4.2

//    Returns \c true if this element is not equal to \a other;
//    otherwise returns \c false.

//    \sa operator==()
//*/

///*!
//    \fn bool MyPainterPath::Element::isCurveTo () const

//    Returns \c true if the element is a curve, otherwise returns \c false.

//    \sa type, MyPainterPath::CurveToElement
//*/

///*!
//    \fn bool MyPainterPath::Element::isLineTo () const

//    Returns \c true if the element is a line, otherwise returns \c false.

//    \sa type, MyPainterPath::LineToElement
//*/

///*!
//    \fn bool MyPainterPath::Element::isMoveTo () const

//    Returns \c true if the element is moving the current position,
//    otherwise returns \c false.

//    \sa type, MyPainterPath::MoveToElement
//*/

///*!
//    \fn MyPainterPath::Element::operator QPointF () const

//    Returns the element's position.

//    \sa x, y
//*/

///*!
//    \fn void MyPainterPath::addEllipse(qreal x, qreal y, qreal width, qreal height)
//    \overload

//    Creates an ellipse within the bounding rectangle defined by its top-left
//    corner at (\a x, \a y), \a width and \a height, and adds it to the
//    painter path as a closed subpath.
//*/

///*!
//    \since 4.4

//    \fn void MyPainterPath::addEllipse(const QPointF &center, qreal rx, qreal ry)
//    \overload

//    Creates an ellipse positioned at \a{center} with radii \a{rx} and \a{ry},
//    and adds it to the painter path as a closed subpath.
//*/

///*!
//    \fn void MyPainterPath::addText(qreal x, qreal y, const QFont &font, const QString &text)
//    \overload

//    Adds the given \a text to this path as a set of closed subpaths created
//    from the \a font supplied. The subpaths are positioned so that the left
//    end of the text's baseline lies at the point specified by (\a x, \a y).
//*/

///*!
//    \fn int MyPainterPath::elementCount() const

//    Returns the number of path elements in the painter path.

//    \sa ElementType, elementAt(), isEmpty()
//*/

//int MyPainterPath::elementCount() const
//{
//    return d_ptr ? d_ptr->elements.size() : 0;
//}

///*!
//    \fn MyPainterPath::Element MyPainterPath::elementAt(int index) const

//    Returns the element at the given \a index in the painter path.

//    \sa ElementType, elementCount(), isEmpty()
//*/

//MyPainterPath::Element MyPainterPath::elementAt(int i) const
//{
//    Q_ASSERT(d_ptr);
//    Q_ASSERT(i >= 0 && i < elementCount());
//    return d_ptr->elements.at(i);
//}

///*!
//    \fn void MyPainterPath::setElementPositionAt(int index, qreal x, qreal y)
//    \since 4.2

//    Sets the x and y coordinate of the element at index \a index to \a
//    x and \a y.
//*/

//void MyPainterPath::setElementPositionAt(int i, qreal x, qreal y)
//{
//    Q_ASSERT(d_ptr);
//    Q_ASSERT(i >= 0 && i < elementCount());
//    detach();
//    MyPainterPath::Element &e = d_ptr->elements[i];
//    e.x = x;
//    e.y = y;
//}


///*###
//    \fn MyPainterPath &MyPainterPath::operator +=(const MyPainterPath &other)

//    Appends the \a other painter path to this painter path and returns a
//    reference to the result.
//*/

///*!
//    Constructs an empty MyPainterPath object.
//*/
//MyPainterPath::MyPainterPath() noexcept
//    : d_ptr(nullptr)
//{
//}


//// TANMAY ADDED

//QPainterPath MyPainterPath::toQPainterPath(){
//    QPainterPath result;
//    for(int i = 0; i < this->elementCount(); i++) {
//        MyPainterPath::Element pp = this->elementAt(i);

//        if (pp.isMoveTo()){
//            result.moveTo(pp.x, pp.y);
//        }
//        else if (pp.isLineTo()) {
//            result.lineTo(pp.x, pp.y);
//        }
//        else if (pp.isCurveTo()) {
//            result.cubicTo(pp.x,pp.y, this->elementAt(i+1).x, this->elementAt(i+1).y, this->elementAt(i+2).x, this->elementAt(i+2).y);
//            i += 2;
//        }

//    }

//    return result;

//}

//// TANMAY ADDED TILL THIS



///*!
//    \fn MyPainterPath::MyPainterPath(const MyPainterPath &path)

//    Creates a MyPainterPath object that is a copy of the given \a path.

//    \sa operator=()
//*/
//MyPainterPath::MyPainterPath(const MyPainterPath &other)
//    : d_ptr(other.d_ptr.data())
//{
//    if (d_ptr)
//        d_ptr->ref.ref();
//}



///*!
//    Creates a MyPainterPath object with the given \a startPoint as its
//    current position.
//*/

//MyPainterPath::MyPainterPath(const QPointF &startPoint)
//    : d_ptr(new MyPainterPathData)
//{
//    Element e = { startPoint.x(), startPoint.y(), MoveToElement };
//    d_func()->elements << e;
//}

//void MyPainterPath::detach()
//{
//#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
//    if (d_ptr->ref.loadRelaxed() != 1)
//#else
//    if (d_ptr->ref.load() != 1)
//#endif
//        detach_helper();
//    setDirty(true);
//}

///*!
//    \internal
//*/
//void MyPainterPath::detach_helper()
//{
//    MyPainterPathPrivate *data = new MyPainterPathData(*d_func());
//    d_ptr.reset(data);
//}

///*!
//    \internal
//*/
//void MyPainterPath::ensureData_helper()
//{
//    MyPainterPathPrivate *data = new MyPainterPathData;
//    data->elements.reserve(16);
//    MyPainterPath::Element e = { 0, 0, MyPainterPath::MoveToElement };
//    data->elements << e;
//    d_ptr.reset(data);
//    Q_ASSERT(d_ptr != nullptr);
//}

///*!
//    \fn MyPainterPath &MyPainterPath::operator=(const MyPainterPath &path)

//    Assigns the given \a path to this painter path.

//    \sa MyPainterPath()
//*/
//MyPainterPath &MyPainterPath::operator=(const MyPainterPath &other)
//{
//    if (other.d_func() != d_func()) {
//        MyPainterPathPrivate *data = other.d_func();
//        if (data)
//            data->ref.ref();
//        d_ptr.reset(data);
//    }
//    return *this;
//}

///*!
//    \fn MyPainterPath &MyPainterPath::operator=(MyPainterPath &&other)

//    Move-assigns \a other to this MyPainterPath instance.

//    \since 5.2
//*/

///*!
//    \fn void MyPainterPath::swap(MyPainterPath &other)
//    \since 4.8

//    Swaps painter path \a other with this painter path. This operation is very
//    fast and never fails.
//*/

///*!
//    Destroys this MyPainterPath object.
//*/
//MyPainterPath::~MyPainterPath()
//{
//}

///*!
//    Clears the path elements stored.

//    This allows the path to reuse previous memory allocations.

//    \sa reserve(), capacity()
//    \since 5.13
//*/
//void MyPainterPath::clear()
//{
//    if (!d_ptr)
//        return;

//    detach();
//    d_func()->clear();
//    d_func()->elements.append( {0, 0, MoveToElement} );
//}

///*!
//    Reserves a given amount of elements in MyPainterPath's internal memory.

//    Attempts to allocate memory for at least \a size elements.

//    \sa clear(), capacity(), QVector::reserve()
//    \since 5.13
//*/
//void MyPainterPath::reserve(int size)
//{
//    Q_D(MyPainterPath);
//    if ((!d && size > 0) || (d && d->elements.capacity() < size)) {
//        ensureData();
//        detach();
//        d_func()->elements.reserve(size);
//    }
//}

///*!
//    Returns the number of elements allocated by the MyPainterPath.

//    \sa clear(), reserve()
//    \since 5.13
//*/
//int MyPainterPath::capacity() const
//{
//    Q_D(MyPainterPath);
//    if (d)
//        return d->elements.capacity();

//    return 0;
//}

///*!
//    Closes the current subpath by drawing a line to the beginning of
//    the subpath, automatically starting a new path. The current point
//    of the new path is (0, 0).

//    If the subpath does not contain any elements, this function does
//    nothing.

//    \sa moveTo(), {MyPainterPath#Composing a MyPainterPath}{Composing
//    a MyPainterPath}
// */
//void MyPainterPath::closeSubpath()
//{
//#ifdef QPP_DEBUG
//    printf("MyPainterPath::closeSubpath()\n");
//#endif
//    if (isEmpty())
//        return;
//    detach();

//    d_func()->close();
//}

///*!
//    \fn void MyPainterPath::moveTo(qreal x, qreal y)

//    \overload

//    Moves the current position to (\a{x}, \a{y}) and starts a new
//    subpath, implicitly closing the previous path.
//*/

///*!
//    \fn void MyPainterPath::moveTo(const QPointF &point)

//    Moves the current point to the given \a point, implicitly starting
//    a new subpath and closing the previous one.

//    \sa closeSubpath(), {MyPainterPath#Composing a
//    MyPainterPath}{Composing a MyPainterPath}
//*/
//void MyPainterPath::moveTo(const QPointF &p)
//{
//#ifdef QPP_DEBUG
//    printf("MyPainterPath::moveTo() (%.2f,%.2f)\n", p.x(), p.y());
//#endif

//    if (!hasValidCoords(p)) {
//#ifndef QT_NO_DEBUG
//        qWarning("MyPainterPath::moveTo: Adding point with invalid coordinates, ignoring call");
//#endif
//        return;
//    }

//    ensureData();
//    detach();

//    MyPainterPathData *d = d_func();
//    Q_ASSERT(!d->elements.isEmpty());

//    d->require_moveTo = false;

//    if (d->elements.constLast().type == MoveToElement) {
//        d->elements.last().x = p.x();
//        d->elements.last().y = p.y();
//    } else {
//        Element elm = { p.x(), p.y(), MoveToElement };
//        d->elements.append(elm);
//    }
//    d->cStart = d->elements.size() - 1;
//}

///*!
//    \fn void MyPainterPath::lineTo(qreal x, qreal y)

//    \overload

//    Draws a line from the current position to the point (\a{x},
//    \a{y}).
//*/

///*!
//    \fn void MyPainterPath::lineTo(const QPointF &endPoint)

//    Adds a straight line from the current position to the given \a
//    endPoint.  After the line is drawn, the current position is updated
//    to be at the end point of the line.

//    \sa addPolygon(), addRect(), {MyPainterPath#Composing a
//    MyPainterPath}{Composing a MyPainterPath}
// */
//void MyPainterPath::lineTo(const QPointF &p)
//{
//#ifdef QPP_DEBUG
//    printf("MyPainterPath::lineTo() (%.2f,%.2f)\n", p.x(), p.y());
//#endif

//    if (!hasValidCoords(p)) {
//#ifndef QT_NO_DEBUG
//        qWarning("MyPainterPath::lineTo: Adding point with invalid coordinates, ignoring call");
//#endif
//        return;
//    }

//    ensureData();
//    detach();

//    MyPainterPathData *d = d_func();
//    Q_ASSERT(!d->elements.isEmpty());
//    d->maybeMoveTo();
//    if (p == QPointF(d->elements.constLast()))
//        return;
//    Element elm = { p.x(), p.y(), LineToElement };
//    d->elements.append(elm);

//    d->convex = d->elements.size() == 3 || (d->elements.size() == 4 && d->isClosed());
//}

///*!
//    \fn void MyPainterPath::cubicTo(qreal c1X, qreal c1Y, qreal c2X,
//    qreal c2Y, qreal endPointX, qreal endPointY);

//    \overload

//    Adds a cubic Bezier curve between the current position and the end
//    point (\a{endPointX}, \a{endPointY}) with control points specified
//    by (\a{c1X}, \a{c1Y}) and (\a{c2X}, \a{c2Y}).
//*/

///*!
//    \fn void MyPainterPath::cubicTo(const QPointF &c1, const QPointF &c2, const QPointF &endPoint)

//    Adds a cubic Bezier curve between the current position and the
//    given \a endPoint using the control points specified by \a c1, and
//    \a c2.

//    After the curve is added, the current position is updated to be at
//    the end point of the curve.

//    \table 100%
//    \row
//    \li \inlineimage MyPainterPath-cubicto.png
//    \li
//    \snippet code/src_gui_painting_MyPainterPath.cpp 1
//    \endtable

//    \sa quadTo(), {MyPainterPath#Composing a MyPainterPath}{Composing
//    a MyPainterPath}
//*/
//void MyPainterPath::cubicTo(const QPointF &c1, const QPointF &c2, const QPointF &e)
//{
//#ifdef QPP_DEBUG
//    printf("MyPainterPath::cubicTo() (%.2f,%.2f), (%.2f,%.2f), (%.2f,%.2f)\n",
//           c1.x(), c1.y(), c2.x(), c2.y(), e.x(), e.y());
//#endif

//    if (!hasValidCoords(c1) || !hasValidCoords(c2) || !hasValidCoords(e)) {
//#ifndef QT_NO_DEBUG
//        qWarning("MyPainterPath::cubicTo: Adding point with invalid coordinates, ignoring call");
//#endif
//        return;
//    }

//    ensureData();
//    detach();

//    MyPainterPathData *d = d_func();
//    Q_ASSERT(!d->elements.isEmpty());


//    // Abort on empty curve as a stroker cannot handle this and the
//    // curve is irrelevant anyway.
//    if (d->elements.constLast() == c1 && c1 == c2 && c2 == e)
//        return;

//    d->maybeMoveTo();

//    Element ce1 = { c1.x(), c1.y(), CurveToElement };
//    Element ce2 = { c2.x(), c2.y(), CurveToDataElement };
//    Element ee = { e.x(), e.y(), CurveToDataElement };
//    d->elements << ce1 << ce2 << ee;
//}

///*!
//    \fn void MyPainterPath::quadTo(qreal cx, qreal cy, qreal endPointX, qreal endPointY);

//    \overload

//    Adds a quadratic Bezier curve between the current point and the endpoint
//    (\a{endPointX}, \a{endPointY}) with the control point specified by
//    (\a{cx}, \a{cy}).
//*/

///*!
//    \fn void MyPainterPath::quadTo(const QPointF &c, const QPointF &endPoint)

//    Adds a quadratic Bezier curve between the current position and the
//    given \a endPoint with the control point specified by \a c.

//    After the curve is added, the current point is updated to be at
//    the end point of the curve.

//    \sa cubicTo(), {MyPainterPath#Composing a MyPainterPath}{Composing a
//    MyPainterPath}
//*/
//void MyPainterPath::quadTo(const QPointF &c, const QPointF &e)
//{
//#ifdef QPP_DEBUG
//    printf("MyPainterPath::quadTo() (%.2f,%.2f), (%.2f,%.2f)\n",
//           c.x(), c.y(), e.x(), e.y());
//#endif

//    if (!hasValidCoords(c) || !hasValidCoords(e)) {
//#ifndef QT_NO_DEBUG
//        qWarning("MyPainterPath::quadTo: Adding point with invalid coordinates, ignoring call");
//#endif
//        return;
//    }

//    ensureData();
//    detach();

//    Q_D(MyPainterPath);
//    Q_ASSERT(!d->elements.isEmpty());
//    const MyPainterPath::Element &elm = d->elements.at(elementCount()-1);
//    QPointF prev(elm.x, elm.y);

//    // Abort on empty curve as a stroker cannot handle this and the
//    // curve is irrelevant anyway.
//    if (prev == c && c == e)
//        return;

//    QPointF c1((prev.x() + 2*c.x()) / 3, (prev.y() + 2*c.y()) / 3);
//    QPointF c2((e.x() + 2*c.x()) / 3, (e.y() + 2*c.y()) / 3);
//    cubicTo(c1, c2, e);
//}

///*!
//    \fn void MyPainterPath::arcTo(qreal x, qreal y, qreal width, qreal
//    height, qreal startAngle, qreal sweepLength)

//    \overload

//    Creates an arc that occupies the rectangle QRectF(\a x, \a y, \a
//    width, \a height), beginning at the specified \a startAngle and
//    extending \a sweepLength degrees counter-clockwise.

//*/

///*!
//    \fn void MyPainterPath::arcTo(const QRectF &rectangle, qreal startAngle, qreal sweepLength)

//    Creates an arc that occupies the given \a rectangle, beginning at
//    the specified \a startAngle and extending \a sweepLength degrees
//    counter-clockwise.

//    Angles are specified in degrees. Clockwise arcs can be specified
//    using negative angles.

//    Note that this function connects the starting point of the arc to
//    the current position if they are not already connected. After the
//    arc has been added, the current position is the last point in
//    arc. To draw a line back to the first point, use the
//    closeSubpath() function.

//    \table 100%
//    \row
//    \li \inlineimage MyPainterPath-arcto.png
//    \li
//    \snippet code/src_gui_painting_MyPainterPath.cpp 2
//    \endtable

//    \sa arcMoveTo(), addEllipse(), QPainter::drawArc(), QPainter::drawPie(),
//    {MyPainterPath#Composing a MyPainterPath}{Composing a
//    MyPainterPath}
//*/
//void MyPainterPath::arcTo(const QRectF &rect, qreal startAngle, qreal sweepLength)
//{
//#ifdef QPP_DEBUG
//    printf("MyPainterPath::arcTo() (%.2f, %.2f, %.2f, %.2f, angle=%.2f, sweep=%.2f\n",
//           rect.x(), rect.y(), rect.width(), rect.height(), startAngle, sweepLength);
//#endif

//    if (!hasValidCoords(rect) || !isValidCoord(startAngle) || !isValidCoord(sweepLength)) {
//#ifndef QT_NO_DEBUG
//        qWarning("MyPainterPath::arcTo: Adding point with invalid coordinates, ignoring call");
//#endif
//        return;
//    }

//    if (rect.isNull())
//        return;

//    ensureData();
//    detach();

//    int point_count;
//    QPointF pts[15];
//    QPointF curve_start = qt_curves_for_arc(rect, startAngle, sweepLength, pts, &point_count);

//    lineTo(curve_start);
//    for (int i=0; i<point_count; i+=3) {
//        cubicTo(pts[i].x(), pts[i].y(),
//                pts[i+1].x(), pts[i+1].y(),
//                pts[i+2].x(), pts[i+2].y());
//    }

//}


///*!
//    \fn void MyPainterPath::arcMoveTo(qreal x, qreal y, qreal width, qreal height, qreal angle)
//    \overload
//    \since 4.2

//    Creates a move to that lies on the arc that occupies the
//    QRectF(\a x, \a y, \a width, \a height) at \a angle.
//*/


///*!
//    \fn void MyPainterPath::arcMoveTo(const QRectF &rectangle, qreal angle)
//    \since 4.2

//    Creates a move to that lies on the arc that occupies the given \a
//    rectangle at \a angle.

//    Angles are specified in degrees. Clockwise arcs can be specified
//    using negative angles.

//    \sa moveTo(), arcTo()
//*/

//void MyPainterPath::arcMoveTo(const QRectF &rect, qreal angle)
//{
//    if (rect.isNull())
//        return;

//    QPointF pt;
//    qt_find_ellipse_coords(rect, angle, 0, &pt, nullptr);
//    moveTo(pt);
//}



///*!
//    \fn QPointF MyPainterPath::currentPosition() const

//    Returns the current position of the path.
//*/
//QPointF MyPainterPath::currentPosition() const
//{
//    return !d_ptr || d_func()->elements.isEmpty()
//        ? QPointF()
//        : QPointF(d_func()->elements.constLast().x, d_func()->elements.constLast().y);
//}


///*!
//    \fn void MyPainterPath::addRect(qreal x, qreal y, qreal width, qreal height)

//    \overload

//    Adds a rectangle at position (\a{x}, \a{y}), with the given \a
//    width and \a height, as a closed subpath.
//*/

///*!
//    \fn void MyPainterPath::addRect(const QRectF &rectangle)

//    Adds the given \a rectangle to this path as a closed subpath.

//    The \a rectangle is added as a clockwise set of lines. The painter
//    path's current position after the \a rectangle has been added is
//    at the top-left corner of the rectangle.

//    \table 100%
//    \row
//    \li \inlineimage MyPainterPath-addrectangle.png
//    \li
//    \snippet code/src_gui_painting_MyPainterPath.cpp 3
//    \endtable

//    \sa addRegion(), lineTo(), {MyPainterPath#Composing a
//    MyPainterPath}{Composing a MyPainterPath}
//*/
//void MyPainterPath::addRect(const QRectF &r)
//{
//    if (!hasValidCoords(r)) {
//#ifndef QT_NO_DEBUG
//        qWarning("MyPainterPath::addRect: Adding point with invalid coordinates, ignoring call");
//#endif
//        return;
//    }

//    if (r.isNull())
//        return;

//    ensureData();
//    detach();

//    bool first = d_func()->elements.size() < 2;

//    moveTo(r.x(), r.y());

//    Element l1 = { r.x() + r.width(), r.y(), LineToElement };
//    Element l2 = { r.x() + r.width(), r.y() + r.height(), LineToElement };
//    Element l3 = { r.x(), r.y() + r.height(), LineToElement };
//    Element l4 = { r.x(), r.y(), LineToElement };

//    d_func()->elements << l1 << l2 << l3 << l4;
//    d_func()->require_moveTo = true;
//    d_func()->convex = first;
//}

///*!
//    Adds the given \a polygon to the path as an (unclosed) subpath.

//    Note that the current position after the polygon has been added,
//    is the last point in \a polygon. To draw a line back to the first
//    point, use the closeSubpath() function.

//    \table 100%
//    \row
//    \li \inlineimage MyPainterPath-addpolygon.png
//    \li
//    \snippet code/src_gui_painting_MyPainterPath.cpp 4
//    \endtable

//    \sa lineTo(), {MyPainterPath#Composing a MyPainterPath}{Composing
//    a MyPainterPath}
//*/
//void MyPainterPath::addPolygon(const QPolygonF &polygon)
//{
//    if (polygon.isEmpty())
//        return;

//    ensureData();
//    detach();

//    moveTo(polygon.constFirst());
//    for (int i=1; i<polygon.size(); ++i) {
//        Element elm = { polygon.at(i).x(), polygon.at(i).y(), LineToElement };
//        d_func()->elements << elm;
//    }
//}

///*!
//    \fn void MyPainterPath::addEllipse(const QRectF &boundingRectangle)

//    Creates an ellipse within the specified \a boundingRectangle
//    and adds it to the painter path as a closed subpath.

//    The ellipse is composed of a clockwise curve, starting and
//    finishing at zero degrees (the 3 o'clock position).

//    \table 100%
//    \row
//    \li \inlineimage MyPainterPath-addellipse.png
//    \li
//    \snippet code/src_gui_painting_MyPainterPath.cpp 5
//    \endtable

//    \sa arcTo(), QPainter::drawEllipse(), {MyPainterPath#Composing a
//    MyPainterPath}{Composing a MyPainterPath}
//*/
//void MyPainterPath::addEllipse(const QRectF &boundingRect)
//{
//    if (!hasValidCoords(boundingRect)) {
//#ifndef QT_NO_DEBUG
//        qWarning("MyPainterPath::addEllipse: Adding point with invalid coordinates, ignoring call");
//#endif
//        return;
//    }

//    if (boundingRect.isNull())
//        return;

//    ensureData();
//    detach();

//    bool first = d_func()->elements.size() < 2;

//    QPointF pts[12];
//    int point_count;
//    QPointF start = qt_curves_for_arc(boundingRect, 0, -360, pts, &point_count);

//    moveTo(start);
//    cubicTo(pts[0], pts[1], pts[2]);           // 0 -> 270
//    cubicTo(pts[3], pts[4], pts[5]);           // 270 -> 180
//    cubicTo(pts[6], pts[7], pts[8]);           // 180 -> 90
//    cubicTo(pts[9], pts[10], pts[11]);         // 90 - >0
//    d_func()->require_moveTo = true;

//    d_func()->convex = first;
//}

///*!
//    \fn void MyPainterPath::addText(const QPointF &point, const QFont &font, const QString &text)

//    Adds the given \a text to this path as a set of closed subpaths
//    created from the \a font supplied. The subpaths are positioned so
//    that the left end of the text's baseline lies at the specified \a
//    point.

//    \table 100%
//    \row
//    \li \inlineimage MyPainterPath-addtext.png
//    \li
//    \snippet code/src_gui_painting_MyPainterPath.cpp 6
//    \endtable

//    \sa QPainter::drawText(), {MyPainterPath#Composing a
//    MyPainterPath}{Composing a MyPainterPath}
//*/














////void MyPainterPath::addText(const QPointF &point, const QFont &f, const QString &text)
////{
////    if (text.isEmpty())
////        return;

////    ensureData();
////    detach();

////    QTextLayout layout(text, f);
////    layout.setCacheEnabled(true);
////    QTextEngine *eng = layout.engine();
////    layout.beginLayout();
////    QTextLine line = layout.createLine();
////    Q_UNUSED(line);
////    layout.endLayout();
////    const QScriptLine &sl = eng->lines[0];
////    if (!sl.length || !eng->layoutData)
////        return;

////    int nItems = eng->layoutData->items.size();

////    qreal x(point.x());
////    qreal y(point.y());

////    QVarLengthArray<int> visualOrder(nItems);
////    QVarLengthArray<uchar> levels(nItems);
////    for (int i = 0; i < nItems; ++i)
////        levels[i] = eng->layoutData->items.at(i).analysis.bidiLevel;
////    QTextEngine::bidiReorder(nItems, levels.data(), visualOrder.data());

////    for (int i = 0; i < nItems; ++i) {
////        int item = visualOrder[i];
////        const QScriptItem &si = eng->layoutData->items.at(item);

////        if (si.analysis.flags < QScriptAnalysis::TabOrObject) {
////            QGlyphLayout glyphs = eng->shapedGlyphs(&si);
////            QFontEngine *fe = f.d->engineForScript(si.analysis.script);
////            Q_ASSERT(fe);
////            fe->addOutlineToPath(x, y, glyphs, this,
////                                 si.analysis.bidiLevel % 2
////                                 ? QTextItem::RenderFlags(QTextItem::RightToLeft)
////                                 : QTextItem::RenderFlags{});

////            const qreal lw = fe->lineThickness().toReal();
////            if (f.d->underline) {
////                qreal pos = fe->underlinePosition().toReal();
////                addRect(x, y + pos, si.width.toReal(), lw);
////            }
////            if (f.d->overline) {
////                qreal pos = fe->ascent().toReal() + 1;
////                addRect(x, y - pos, si.width.toReal(), lw);
////            }
////            if (f.d->strikeOut) {
////                qreal pos = fe->ascent().toReal() / 3;
////                addRect(x, y - pos, si.width.toReal(), lw);
////            }
////        }
////        x += si.width.toReal();
////    }
////}














///*!
//    \fn void MyPainterPath::addPath(const MyPainterPath &path)

//    Adds the given \a path to \e this path as a closed subpath.

//    \sa connectPath(), {MyPainterPath#Composing a
//    MyPainterPath}{Composing a MyPainterPath}
//*/
//void MyPainterPath::addPath(const MyPainterPath &other)
//{
//    if (other.isEmpty())
//        return;

//    ensureData();
//    detach();

//    MyPainterPathData *d = reinterpret_cast<MyPainterPathData *>(d_func());
//    // Remove last moveto so we don't get multiple moveto's
//    if (d->elements.constLast().type == MoveToElement)
//        d->elements.remove(d->elements.size()-1);

//    // Locate where our own current subpath will start after the other path is added.
//    int cStart = d->elements.size() + other.d_func()->cStart;
//    d->elements += other.d_func()->elements;
//    d->cStart = cStart;

//    d->require_moveTo = other.d_func()->isClosed();
//}


///*!
//    \fn void MyPainterPath::connectPath(const MyPainterPath &path)

//    Connects the given \a path to \e this path by adding a line from the
//    last element of this path to the first element of the given path.

//    \sa addPath(), {MyPainterPath#Composing a MyPainterPath}{Composing
//    a MyPainterPath}
//*/
//void MyPainterPath::connectPath(const MyPainterPath &other)
//{
//    if (other.isEmpty())
//        return;

//    ensureData();
//    detach();

//    MyPainterPathData *d = reinterpret_cast<MyPainterPathData *>(d_func());
//    // Remove last moveto so we don't get multiple moveto's
//    if (d->elements.constLast().type == MoveToElement)
//        d->elements.remove(d->elements.size()-1);

//    // Locate where our own current subpath will start after the other path is added.
//    int cStart = d->elements.size() + other.d_func()->cStart;
//    int first = d->elements.size();
//    d->elements += other.d_func()->elements;

//    if (first != 0)
//        d->elements[first].type = LineToElement;

//    // avoid duplicate points
//    if (first > 0 && QPointF(d->elements.at(first)) == QPointF(d->elements.at(first - 1))) {
//        d->elements.remove(first--);
//        --cStart;
//    }

//    if (cStart != first)
//        d->cStart = cStart;
//}

///*!
//    Adds the given \a region to the path by adding each rectangle in
//    the region as a separate closed subpath.

//    \sa addRect(), {MyPainterPath#Composing a MyPainterPath}{Composing
//    a MyPainterPath}
//*/
//void MyPainterPath::addRegion(const QRegion &region)
//{
//    ensureData();
//    detach();

//    for (const QRect &rect : region)
//        addRect(rect);
//}


///*!
//    Returns the painter path's currently set fill rule.

//    \sa setFillRule()
//*/
//Qt::FillRule MyPainterPath::fillRule() const
//{
//    return isEmpty() ? Qt::OddEvenFill : d_func()->fillRule;
//}

///*!
//    \fn void MyPainterPath::setFillRule(Qt::FillRule fillRule)

//    Sets the fill rule of the painter path to the given \a
//    fillRule. Qt provides two methods for filling paths:

//    \table
//    \header
//    \li Qt::OddEvenFill (default)
//    \li Qt::WindingFill
//    \row
//    \li \inlineimage qt-fillrule-oddeven.png
//    \li \inlineimage qt-fillrule-winding.png
//    \endtable

//    \sa fillRule()
//*/
//void MyPainterPath::setFillRule(Qt::FillRule fillRule)
//{
//    ensureData();
//    if (d_func()->fillRule == fillRule)
//        return;
//    detach();

//    d_func()->fillRule = fillRule;
//}

//#define QT_BEZIER_A(bezier, coord) 3 * (-bezier.coord##1 \
//                                        + 3*bezier.coord##2 \
//                                        - 3*bezier.coord##3 \
//                                        +bezier.coord##4)

//#define QT_BEZIER_B(bezier, coord) 6 * (bezier.coord##1 \
//                                        - 2*bezier.coord##2 \
//                                        + bezier.coord##3)

//#define QT_BEZIER_C(bezier, coord) 3 * (- bezier.coord##1 \
//                                        + bezier.coord##2)

//#define QT_BEZIER_CHECK_T(bezier, t) \
//    if (t >= 0 && t <= 1) { \
//        QPointF p(b.pointAt(t)); \
//        if (p.x() < minx) minx = p.x(); \
//        else if (p.x() > maxx) maxx = p.x(); \
//        if (p.y() < miny) miny = p.y(); \
//        else if (p.y() > maxy) maxy = p.y(); \
//    }


//static QRectF qt_painterpath_bezier_extrema(const QBezier &b)
//{
//    qreal minx, miny, maxx, maxy;

//    // initialize with end points
//    if (b.x1 < b.x4) {
//        minx = b.x1;
//        maxx = b.x4;
//    } else {
//        minx = b.x4;
//        maxx = b.x1;
//    }
//    if (b.y1 < b.y4) {
//        miny = b.y1;
//        maxy = b.y4;
//    } else {
//        miny = b.y4;
//        maxy = b.y1;
//    }

//    // Update for the X extrema
//    {
//        qreal ax = QT_BEZIER_A(b, x);
//        qreal bx = QT_BEZIER_B(b, x);
//        qreal cx = QT_BEZIER_C(b, x);
//        // specialcase quadratic curves to avoid div by zero
//        if (qFuzzyIsNull(ax)) {

//            // linear curves are covered by initialization.
//            if (!qFuzzyIsNull(bx)) {
//                qreal t = -cx / bx;
//                QT_BEZIER_CHECK_T(b, t);
//            }

//        } else {
//            const qreal tx = bx * bx - 4 * ax * cx;

//            if (tx >= 0) {
//                qreal temp = qSqrt(tx);
//                qreal rcp = 1 / (2 * ax);
//                qreal t1 = (-bx + temp) * rcp;
//                QT_BEZIER_CHECK_T(b, t1);

//                qreal t2 = (-bx - temp) * rcp;
//                QT_BEZIER_CHECK_T(b, t2);
//            }
//        }
//    }

//    // Update for the Y extrema
//    {
//        qreal ay = QT_BEZIER_A(b, y);
//        qreal by = QT_BEZIER_B(b, y);
//        qreal cy = QT_BEZIER_C(b, y);

//        // specialcase quadratic curves to avoid div by zero
//        if (qFuzzyIsNull(ay)) {

//            // linear curves are covered by initialization.
//            if (!qFuzzyIsNull(by)) {
//                qreal t = -cy / by;
//                QT_BEZIER_CHECK_T(b, t);
//            }

//        } else {
//            const qreal ty = by * by - 4 * ay * cy;

//            if (ty > 0) {
//                qreal temp = qSqrt(ty);
//                qreal rcp = 1 / (2 * ay);
//                qreal t1 = (-by + temp) * rcp;
//                QT_BEZIER_CHECK_T(b, t1);

//                qreal t2 = (-by - temp) * rcp;
//                QT_BEZIER_CHECK_T(b, t2);
//            }
//        }
//    }
//    return QRectF(minx, miny, maxx - minx, maxy - miny);
//}

///*!
//    Returns the bounding rectangle of this painter path as a rectangle with
//    floating point precision.

//    \sa controlPointRect()
//*/
//QRectF MyPainterPath::boundingRect() const
//{
//    if (!d_ptr)
//        return QRectF();
//    MyPainterPathData *d = d_func();

//    if (d->dirtyBounds)
//        computeBoundingRect();
//    return d->bounds;
//}

///*!
//    Returns the rectangle containing all the points and control points
//    in this path.

//    This function is significantly faster to compute than the exact
//    boundingRect(), and the returned rectangle is always a superset of
//    the rectangle returned by boundingRect().

//    \sa boundingRect()
//*/
//QRectF MyPainterPath::controlPointRect() const
//{
//    if (!d_ptr)
//        return QRectF();
//    MyPainterPathData *d = d_func();

//    if (d->dirtyControlBounds)
//        computeControlPointRect();
//    return d->controlBounds;
//}


///*!
//    \fn bool MyPainterPath::isEmpty() const

//    Returns \c true if either there are no elements in this path, or if the only
//    element is a MoveToElement; otherwise returns \c false.

//    \sa elementCount()
//*/

//bool MyPainterPath::isEmpty() const
//{
//    return !d_ptr || (d_ptr->elements.size() == 1 && d_ptr->elements.first().type == MoveToElement);
//}

///*!
//    Creates and returns a reversed copy of the path.

//    It is the order of the elements that is reversed: If a
//    MyPainterPath is composed by calling the moveTo(), lineTo() and
//    cubicTo() functions in the specified order, the reversed copy is
//    composed by calling cubicTo(), lineTo() and moveTo().
//*/
//MyPainterPath MyPainterPath::toReversed() const
//{
//    Q_D(const MyPainterPath);
//    MyPainterPath rev;

//    if (isEmpty()) {
//        rev = *this;
//        return rev;
//    }

//    rev.moveTo(d->elements.at(d->elements.size()-1).x, d->elements.at(d->elements.size()-1).y);

//    for (int i=d->elements.size()-1; i>=1; --i) {
//        const MyPainterPath::Element &elm = d->elements.at(i);
//        const MyPainterPath::Element &prev = d->elements.at(i-1);
//        switch (elm.type) {
//        case LineToElement:
//            rev.lineTo(prev.x, prev.y);
//            break;
//        case MoveToElement:
//            rev.moveTo(prev.x, prev.y);
//            break;
//        case CurveToDataElement:
//            {
//                Q_ASSERT(i>=3);
//                const MyPainterPath::Element &cp1 = d->elements.at(i-2);
//                const MyPainterPath::Element &sp = d->elements.at(i-3);
//                Q_ASSERT(prev.type == CurveToDataElement);
//                Q_ASSERT(cp1.type == CurveToElement);
//                rev.cubicTo(prev.x, prev.y, cp1.x, cp1.y, sp.x, sp.y);
//                i -= 2;
//                break;
//            }
//        default:
//            Q_ASSERT(!"qt_reversed_path");
//            break;
//        }
//    }
//    //qt_debug_path(rev);
//    return rev;
//}

///*!
//    Converts the path into a list of polygons using the QTransform
//    \a matrix, and returns the list.

//    This function creates one polygon for each subpath regardless of
//    intersecting subpaths (i.e. overlapping bounding rectangles). To
//    make sure that such overlapping subpaths are filled correctly, use
//    the toFillPolygons() function instead.

//    \sa toFillPolygons(), toFillPolygon(), {MyPainterPath#MyPainterPath
//    Conversion}{MyPainterPath Conversion}
//*/
//QList<QPolygonF> MyPainterPath::toSubpathPolygons(const QTransform &matrix) const
//{

//    Q_D(const MyPainterPath);
//    QList<QPolygonF> flatCurves;
//    if (isEmpty())
//        return flatCurves;

//    QPolygonF current;
//    for (int i=0; i<elementCount(); ++i) {
//        const MyPainterPath::Element &e = d->elements.at(i);
//        switch (e.type) {
//        case MyPainterPath::MoveToElement:
//            if (current.size() > 1)
//                flatCurves += current;
//            current.clear();
//            current.reserve(16);
//            current += QPointF(e.x, e.y) * matrix;
//            break;
//        case MyPainterPath::LineToElement:
//            current += QPointF(e.x, e.y) * matrix;
//            break;
//        case MyPainterPath::CurveToElement: {
//            Q_ASSERT(d->elements.at(i+1).type == MyPainterPath::CurveToDataElement);
//            Q_ASSERT(d->elements.at(i+2).type == MyPainterPath::CurveToDataElement);
//            QBezier bezier = QBezier::fromPoints(QPointF(d->elements.at(i-1).x, d->elements.at(i-1).y) * matrix,
//                                       QPointF(e.x, e.y) * matrix,
//                                       QPointF(d->elements.at(i+1).x, d->elements.at(i+1).y) * matrix,
//                                                 QPointF(d->elements.at(i+2).x, d->elements.at(i+2).y) * matrix);
//            bezier.addToPolygon(&current);
//            i+=2;
//            break;
//        }
//        case MyPainterPath::CurveToDataElement:
//            Q_ASSERT(!"MyPainterPath::toSubpathPolygons(), bad element type");
//            break;
//        }
//    }

//    if (current.size()>1)
//        flatCurves += current;

//    return flatCurves;
//}

//#if QT_DEPRECATED_SINCE(5, 15)
///*!
//  \overload
//  \obsolete

//  Use toSubpathPolygons(const QTransform &matrix) instead.
// */
//QList<QPolygonF> MyPainterPath::toSubpathPolygons(const QMatrix &matrix) const
//{
//    return toSubpathPolygons(QTransform(matrix));
//}
//#endif // QT_DEPRECATED_SINCE(5, 15)

///*!
//    Converts the path into a list of polygons using the
//    QTransform \a matrix, and returns the list.

//    The function differs from the toFillPolygon() function in that it
//    creates several polygons. It is provided because it is usually
//    faster to draw several small polygons than to draw one large
//    polygon, even though the total number of points drawn is the same.

//    The toFillPolygons() function differs from the toSubpathPolygons()
//    function in that it create only polygon for subpaths that have
//    overlapping bounding rectangles.

//    Like the toFillPolygon() function, this function uses a rewinding
//    technique to make sure that overlapping subpaths can be filled
//    using the correct fill rule. Note that rewinding inserts addition
//    lines in the polygons so the outline of the fill polygon does not
//    match the outline of the path.

//    \sa toSubpathPolygons(), toFillPolygon(),
//    {MyPainterPath#MyPainterPath Conversion}{MyPainterPath Conversion}
//*/
//QList<QPolygonF> MyPainterPath::toFillPolygons(const QTransform &matrix) const
//{

//    QList<QPolygonF> polys;

//    QList<QPolygonF> subpaths = toSubpathPolygons(matrix);
//    int count = subpaths.size();

//    if (count == 0)
//        return polys;

//    QVector<QRectF> bounds;
//    bounds.reserve(count);
//    for (int i=0; i<count; ++i)
//        bounds += subpaths.at(i).boundingRect();

//#ifdef QPP_FILLPOLYGONS_DEBUG
//    printf("MyPainterPath::toFillPolygons, subpathCount=%d\n", count);
//    for (int i=0; i<bounds.size(); ++i)
//        qDebug() << " bounds" << i << bounds.at(i);
//#endif

//    QVector< QVector<int> > isects;
//    isects.resize(count);

//    // find all intersections
//    for (int j=0; j<count; ++j) {
//        if (subpaths.at(j).size() <= 2)
//            continue;
//        QRectF cbounds = bounds.at(j);
//        for (int i=0; i<count; ++i) {
//            if (cbounds.intersects(bounds.at(i))) {
//                isects[j] << i;
//            }
//        }
//    }

//#ifdef QPP_FILLPOLYGONS_DEBUG
//    printf("Intersections before flattening:\n");
//    for (int i = 0; i < count; ++i) {
//        printf("%d: ", i);
//        for (int j = 0; j < isects[i].size(); ++j) {
//            printf("%d ", isects[i][j]);
//        }
//        printf("\n");
//    }
//#endif

//    // flatten the sets of intersections
//    for (int i=0; i<count; ++i) {
//        const QVector<int> &current_isects = isects.at(i);
//        for (int j=0; j<current_isects.size(); ++j) {
//            int isect_j = current_isects.at(j);
//            if (isect_j == i)
//                continue;
//            const QVector<int> &isects_j = isects.at(isect_j);
//            for (int k = 0, size = isects_j.size(); k < size; ++k) {
//                int isect_k = isects_j.at(k);
//                if (isect_k != i && !isects.at(i).contains(isect_k)) {
//                    isects[i] += isect_k;
//                }
//            }
//            isects[isect_j].clear();
//        }
//    }

//#ifdef QPP_FILLPOLYGONS_DEBUG
//    printf("Intersections after flattening:\n");
//    for (int i = 0; i < count; ++i) {
//        printf("%d: ", i);
//        for (int j = 0; j < isects[i].size(); ++j) {
//            printf("%d ", isects[i][j]);
//        }
//        printf("\n");
//    }
//#endif

//    // Join the intersected subpaths as rewinded polygons
//    for (int i=0; i<count; ++i) {
//        const QVector<int> &subpath_list = isects.at(i);
//        if (!subpath_list.isEmpty()) {
//            QPolygonF buildUp;
//            for (int j=0; j<subpath_list.size(); ++j) {
//                const QPolygonF &subpath = subpaths.at(subpath_list.at(j));
//                buildUp += subpath;
//                if (!subpath.isClosed())
//                    buildUp += subpath.first();
//                if (!buildUp.isClosed())
//                    buildUp += buildUp.constFirst();
//            }
//            polys += buildUp;
//        }
//    }

//    return polys;
//}

//#if QT_DEPRECATED_SINCE(5, 15)
///*!
//  \overload
//  \obsolete

//  Use toFillPolygons(const QTransform &matrix) instead.
// */
//QList<QPolygonF> MyPainterPath::toFillPolygons(const QMatrix &matrix) const
//{
//    return toFillPolygons(QTransform(matrix));
//}
//#endif // QT_DEPRECATED_SINCE(5, 15)

////same as qt_polygon_isect_line in qpolygon.cpp
//static void qt_painterpath_isect_line(const QPointF &p1,
//                                      const QPointF &p2,
//                                      const QPointF &pos,
//                                      int *winding)
//{
//    qreal x1 = p1.x();
//    qreal y1 = p1.y();
//    qreal x2 = p2.x();
//    qreal y2 = p2.y();
//    qreal y = pos.y();

//    int dir = 1;

//    if (qFuzzyCompare(y1, y2)) {
//        // ignore horizontal lines according to scan conversion rule
//        return;
//    } else if (y2 < y1) {
//        qreal x_tmp = x2; x2 = x1; x1 = x_tmp;
//        qreal y_tmp = y2; y2 = y1; y1 = y_tmp;
//        dir = -1;
//    }

//    if (y >= y1 && y < y2) {
//        qreal x = x1 + ((x2 - x1) / (y2 - y1)) * (y - y1);

//        // count up the winding number if we're
//        if (x<=pos.x()) {
//            (*winding) += dir;
//        }
//    }
//}

//static void qt_painterpath_isect_curve(const QBezier &bezier, const QPointF &pt,
//                                       int *winding, int depth = 0)
//{
//    qreal y = pt.y();
//    qreal x = pt.x();
//    QRectF bounds = bezier.bounds();

//    // potential intersection, divide and try again...
//    // Please note that a sideeffect of the bottom exclusion is that
//    // horizontal lines are dropped, but this is correct according to
//    // scan conversion rules.
//    if (y >= bounds.y() && y < bounds.y() + bounds.height()) {

//        // hit lower limit... This is a rough threshold, but its a
//        // tradeoff between speed and precision.
//        const qreal lower_bound = qreal(.001);
//        if (depth == 32 || (bounds.width() < lower_bound && bounds.height() < lower_bound)) {
//            // We make the assumption here that the curve starts to
//            // approximate a line after while (i.e. that it doesn't
//            // change direction drastically during its slope)
//            if (bezier.pt1().x() <= x) {
//                (*winding) += (bezier.pt4().y() > bezier.pt1().y() ? 1 : -1);
//            }
//            return;
//        }

//        // split curve and try again...
//        const auto halves = bezier.split();
//        qt_painterpath_isect_curve(halves.first,  pt, winding, depth + 1);
//        qt_painterpath_isect_curve(halves.second, pt, winding, depth + 1);
//    }
//}

///*!
//    \fn bool MyPainterPath::contains(const QPointF &point) const

//    Returns \c true if the given \a point is inside the path, otherwise
//    returns \c false.

//    \sa intersects()
//*/
//bool MyPainterPath::contains(const QPointF &pt) const
//{
//    if (isEmpty() || !controlPointRect().contains(pt))
//        return false;

//    MyPainterPathData *d = d_func();

//    int winding_number = 0;

//    QPointF last_pt;
//    QPointF last_start;
//    for (int i=0; i<d->elements.size(); ++i) {
//        const Element &e = d->elements.at(i);

//        switch (e.type) {

//        case MoveToElement:
//            if (i > 0) // implicitly close all paths.
//                qt_painterpath_isect_line(last_pt, last_start, pt, &winding_number);
//            last_start = last_pt = e;
//            break;

//        case LineToElement:
//            qt_painterpath_isect_line(last_pt, e, pt, &winding_number);
//            last_pt = e;
//            break;

//        case CurveToElement:
//            {
//                const MyPainterPath::Element &cp2 = d->elements.at(++i);
//                const MyPainterPath::Element &ep = d->elements.at(++i);
//                qt_painterpath_isect_curve(QBezier::fromPoints(last_pt, e, cp2, ep),
//                                           pt, &winding_number);
//                last_pt = ep;

//            }
//            break;

//        default:
//            break;
//        }
//    }

//    // implicitly close last subpath
//    if (last_pt != last_start)
//        qt_painterpath_isect_line(last_pt, last_start, pt, &winding_number);

//    return (d->fillRule == Qt::WindingFill
//            ? (winding_number != 0)
//            : ((winding_number % 2) != 0));
//}

//enum PainterDirections { Left, Right, Top, Bottom };

//static bool qt_painterpath_isect_line_rect(qreal x1, qreal y1, qreal x2, qreal y2,
//                                           const QRectF &rect)
//{
//    qreal left = rect.left();
//    qreal right = rect.right();
//    qreal top = rect.top();
//    qreal bottom = rect.bottom();

//    // clip the lines, after cohen-sutherland, see e.g. http://www.nondot.org/~sabre/graphpro/line6.html
//    int p1 = ((x1 < left) << Left)
//             | ((x1 > right) << Right)
//             | ((y1 < top) << Top)
//             | ((y1 > bottom) << Bottom);
//    int p2 = ((x2 < left) << Left)
//             | ((x2 > right) << Right)
//             | ((y2 < top) << Top)
//             | ((y2 > bottom) << Bottom);

//    if (p1 & p2)
//        // completely inside
//        return false;

//    if (p1 | p2) {
//        qreal dx = x2 - x1;
//        qreal dy = y2 - y1;

//        // clip x coordinates
//        if (x1 < left) {
//            y1 += dy/dx * (left - x1);
//            x1 = left;
//        } else if (x1 > right) {
//            y1 -= dy/dx * (x1 - right);
//            x1 = right;
//        }
//        if (x2 < left) {
//            y2 += dy/dx * (left - x2);
//            x2 = left;
//        } else if (x2 > right) {
//            y2 -= dy/dx * (x2 - right);
//            x2 = right;
//        }

//        p1 = ((y1 < top) << Top)
//             | ((y1 > bottom) << Bottom);
//        p2 = ((y2 < top) << Top)
//             | ((y2 > bottom) << Bottom);

//        if (p1 & p2)
//            return false;

//        // clip y coordinates
//        if (y1 < top) {
//            x1 += dx/dy * (top - y1);
//            y1 = top;
//        } else if (y1 > bottom) {
//            x1 -= dx/dy * (y1 - bottom);
//            y1 = bottom;
//        }
//        if (y2 < top) {
//            x2 += dx/dy * (top - y2);
//            y2 = top;
//        } else if (y2 > bottom) {
//            x2 -= dx/dy * (y2 - bottom);
//            y2 = bottom;
//        }

//        p1 = ((x1 < left) << Left)
//             | ((x1 > right) << Right);
//        p2 = ((x2 < left) << Left)
//             | ((x2 > right) << Right);

//        if (p1 & p2)
//            return false;

//        return true;
//    }
//    return false;
//}

//static bool qt_isect_curve_horizontal(const QBezier &bezier, qreal y, qreal x1, qreal x2, int depth = 0)
//{
//    QRectF bounds = bezier.bounds();

//    if (y >= bounds.top() && y < bounds.bottom()
//        && bounds.right() >= x1 && bounds.left() < x2) {
//        const qreal lower_bound = qreal(.01);
//        if (depth == 32 || (bounds.width() < lower_bound && bounds.height() < lower_bound))
//            return true;

//        const auto halves = bezier.split();
//        if (qt_isect_curve_horizontal(halves.first, y, x1, x2, depth + 1)
//            || qt_isect_curve_horizontal(halves.second, y, x1, x2, depth + 1))
//            return true;
//    }
//    return false;
//}

//static bool qt_isect_curve_vertical(const QBezier &bezier, qreal x, qreal y1, qreal y2, int depth = 0)
//{
//    QRectF bounds = bezier.bounds();

//    if (x >= bounds.left() && x < bounds.right()
//        && bounds.bottom() >= y1 && bounds.top() < y2) {
//        const qreal lower_bound = qreal(.01);
//        if (depth == 32 || (bounds.width() < lower_bound && bounds.height() < lower_bound))
//            return true;

//        const auto halves = bezier.split();
//        if (qt_isect_curve_vertical(halves.first, x, y1, y2, depth + 1)
//            || qt_isect_curve_vertical(halves.second, x, y1, y2, depth + 1))
//            return true;
//    }
//     return false;
//}

//static bool pointOnEdge(const QRectF &rect, const QPointF &point)
//{
//    if ((point.x() == rect.left() || point.x() == rect.right()) &&
//        (point.y() >= rect.top() && point.y() <= rect.bottom()))
//        return true;
//    if ((point.y() == rect.top() || point.y() == rect.bottom()) &&
//        (point.x() >= rect.left() && point.x() <= rect.right()))
//        return true;
//    return false;
//}

///*
//    Returns \c true if any lines or curves cross the four edges in of rect
//*/
//static bool qt_painterpath_check_crossing(const MyPainterPath *path, const QRectF &rect)
//{
//    QPointF last_pt;
//    QPointF last_start;
//    enum { OnRect, InsideRect, OutsideRect} edgeStatus = OnRect;
//    for (int i=0; i<path->elementCount(); ++i) {
//        const MyPainterPath::Element &e = path->elementAt(i);

//        switch (e.type) {

//        case MyPainterPath::MoveToElement:
//            if (i > 0
//                && qFuzzyCompare(last_pt.x(), last_start.x())
//                && qFuzzyCompare(last_pt.y(), last_start.y())
//                && qt_painterpath_isect_line_rect(last_pt.x(), last_pt.y(),
//                                                  last_start.x(), last_start.y(), rect))
//                return true;
//            last_start = last_pt = e;
//            break;

//        case MyPainterPath::LineToElement:
//            if (qt_painterpath_isect_line_rect(last_pt.x(), last_pt.y(), e.x, e.y, rect))
//                return true;
//            last_pt = e;
//            break;

//        case MyPainterPath::CurveToElement:
//            {
//                QPointF cp2 = path->elementAt(++i);
//                QPointF ep = path->elementAt(++i);
//                QBezier bezier = QBezier::fromPoints(last_pt, e, cp2, ep);
//                if (qt_isect_curve_horizontal(bezier, rect.top(), rect.left(), rect.right())
//                    || qt_isect_curve_horizontal(bezier, rect.bottom(), rect.left(), rect.right())
//                    || qt_isect_curve_vertical(bezier, rect.left(), rect.top(), rect.bottom())
//                    || qt_isect_curve_vertical(bezier, rect.right(), rect.top(), rect.bottom()))
//                    return true;
//                last_pt = ep;
//            }
//            break;

//        default:
//            break;
//        }
//        // Handle crossing the edges of the rect at the end-points of individual sub-paths.
//        // A point on on the edge itself is considered neither inside nor outside for this purpose.
//        if (!pointOnEdge(rect, last_pt)) {
//            bool contained = rect.contains(last_pt);
//            switch (edgeStatus) {
//            case OutsideRect:
//                if (contained)
//                    return true;
//                break;
//            case InsideRect:
//                if (!contained)
//                    return true;
//                break;
//            case OnRect:
//                edgeStatus = contained ? InsideRect : OutsideRect;
//                break;
//            }
//        } else {
//            if (last_pt == last_start)
//                edgeStatus = OnRect;
//        }
//    }

//    // implicitly close last subpath
//    if (last_pt != last_start
//        && qt_painterpath_isect_line_rect(last_pt.x(), last_pt.y(),
//                                          last_start.x(), last_start.y(), rect))
//        return true;

//    return false;
//}

///*!
//    \fn bool MyPainterPath::intersects(const QRectF &rectangle) const

//    Returns \c true if any point in the given \a rectangle intersects the
//    path; otherwise returns \c false.

//    There is an intersection if any of the lines making up the
//    rectangle crosses a part of the path or if any part of the
//    rectangle overlaps with any area enclosed by the path. This
//    function respects the current fillRule to determine what is
//    considered inside the path.

//    \sa contains()
//*/
//bool MyPainterPath::intersects(const QRectF &rect) const
//{
//    if (elementCount() == 1 && rect.contains(elementAt(0)))
//        return true;

//    if (isEmpty())
//        return false;

//    QRectF cp = controlPointRect();
//    QRectF rn = rect.normalized();

//    // QRectF::intersects returns false if one of the rects is a null rect
//    // which would happen for a painter path consisting of a vertical or
//    // horizontal line
//    if (qMax(rn.left(), cp.left()) > qMin(rn.right(), cp.right())
//        || qMax(rn.top(), cp.top()) > qMin(rn.bottom(), cp.bottom()))
//        return false;

//    // If any path element cross the rect its bound to be an intersection
//    if (qt_painterpath_check_crossing(this, rect))
//        return true;

//    if (contains(rect.center()))
//        return true;

//    Q_D(MyPainterPath);

//    // Check if the rectangle surounds any subpath...
//    for (int i=0; i<d->elements.size(); ++i) {
//        const Element &e = d->elements.at(i);
//        if (e.type == MyPainterPath::MoveToElement && rect.contains(e))
//            return true;
//    }

//    return false;
//}

///*!
//    Translates all elements in the path by (\a{dx}, \a{dy}).

//    \since 4.6
//    \sa translated()
//*/
//void MyPainterPath::translate(qreal dx, qreal dy)
//{
//    if (!d_ptr || (dx == 0 && dy == 0))
//        return;

//    int elementsLeft = d_ptr->elements.size();
//    if (elementsLeft <= 0)
//        return;

//    detach();
//    MyPainterPath::Element *element = d_func()->elements.data();
//    Q_ASSERT(element);
//    while (elementsLeft--) {
//        element->x += dx;
//        element->y += dy;
//        ++element;
//    }
//}

///*!
//    \fn void MyPainterPath::translate(const QPointF &offset)
//    \overload
//    \since 4.6

//    Translates all elements in the path by the given \a offset.

//    \sa translated()
//*/

///*!
//    Returns a copy of the path that is translated by (\a{dx}, \a{dy}).

//    \since 4.6
//    \sa translate()
//*/
//MyPainterPath MyPainterPath::translated(qreal dx, qreal dy) const
//{
//    MyPainterPath copy(*this);
//    copy.translate(dx, dy);
//    return copy;
//}

///*!
//    \fn MyPainterPath MyPainterPath::translated(const QPointF &offset) const;
//    \overload
//    \since 4.6

//    Returns a copy of the path that is translated by the given \a offset.

//    \sa translate()
//*/

///*!
//    \fn bool MyPainterPath::contains(const QRectF &rectangle) const

//    Returns \c true if the given \a rectangle is inside the path,
//    otherwise returns \c false.
//*/
//bool MyPainterPath::contains(const QRectF &rect) const
//{
//    Q_D(MyPainterPath);

//    // the path is empty or the control point rect doesn't completely
//    // cover the rectangle we abort stratight away.
//    if (isEmpty() || !controlPointRect().contains(rect))
//        return false;

//    // if there are intersections, chances are that the rect is not
//    // contained, except if we have winding rule, in which case it
//    // still might.
//    if (qt_painterpath_check_crossing(this, rect)) {
//        if (fillRule() == Qt::OddEvenFill) {
//            return false;
//        } else {
//            // Do some wague sampling in the winding case. This is not
//            // precise but it should mostly be good enough.
//            if (!contains(rect.topLeft()) ||
//                !contains(rect.topRight()) ||
//                !contains(rect.bottomRight()) ||
//                !contains(rect.bottomLeft()))
//                return false;
//        }
//    }

//    // If there exists a point inside that is not part of the path its
//    // because: rectangle lies completely outside path or a subpath
//    // excludes parts of the rectangle. Both cases mean that the rect
//    // is not contained
//    if (!contains(rect.center()))
//        return false;

//    // If there are any subpaths inside this rectangle we need to
//    // check if they are still contained as a result of the fill
//    // rule. This can only be the case for WindingFill though. For
//    // OddEvenFill the rect will never be contained if it surrounds a
//    // subpath. (the case where two subpaths are completely identical
//    // can be argued but we choose to neglect it).
//    for (int i=0; i<d->elements.size(); ++i) {
//        const Element &e = d->elements.at(i);
//        if (e.type == MyPainterPath::MoveToElement && rect.contains(e)) {
//            if (fillRule() == Qt::OddEvenFill)
//                return false;

//            bool stop = false;
//            for (; !stop && i<d->elements.size(); ++i) {
//                const Element &el = d->elements.at(i);
//                switch (el.type) {
//                case MoveToElement:
//                    stop = true;
//                    break;
//                case LineToElement:
//                    if (!contains(el))
//                        return false;
//                    break;
//                case CurveToElement:
//                    if (!contains(d->elements.at(i+2)))
//                        return false;
//                    i += 2;
//                    break;
//                default:
//                    break;
//                }
//            }

//            // compensate for the last ++i in the inner for
//            --i;
//        }
//    }

//    return true;
//}

//static inline bool epsilonCompare(const QPointF &a, const QPointF &b, const QSizeF &epsilon)
//{
//    return qAbs(a.x() - b.x()) <= epsilon.width()
//        && qAbs(a.y() - b.y()) <= epsilon.height();
//}

///*!
//    Returns \c true if this painterpath is equal to the given \a path.

//    Note that comparing paths may involve a per element comparison
//    which can be slow for complex paths.

//    \sa operator!=()
//*/

//bool MyPainterPath::operator==(const MyPainterPath &path) const
//{
//    MyPainterPathData *d = reinterpret_cast<MyPainterPathData *>(d_func());
//    MyPainterPathData *other_d = path.d_func();
//    if (other_d == d) {
//        return true;
//    } else if (!d || !other_d) {
//        if (!other_d && isEmpty() && elementAt(0) == QPointF() && d->fillRule == Qt::OddEvenFill)
//            return true;
//        if (!d && path.isEmpty() && path.elementAt(0) == QPointF() && other_d->fillRule == Qt::OddEvenFill)
//            return true;
//        return false;
//    }
//    else if (d->fillRule != other_d->fillRule)
//        return false;
//    else if (d->elements.size() != other_d->elements.size())
//        return false;

//    const qreal qt_epsilon = sizeof(qreal) == sizeof(double) ? 1e-12 : qreal(1e-5);

//    QSizeF epsilon = boundingRect().size();
//    epsilon.rwidth() *= qt_epsilon;
//    epsilon.rheight() *= qt_epsilon;

//    for (int i = 0; i < d->elements.size(); ++i)
//        if (d->elements.at(i).type != other_d->elements.at(i).type
//            || !epsilonCompare(d->elements.at(i), other_d->elements.at(i), epsilon))
//            return false;

//    return true;
//}

///*!
//    Returns \c true if this painter path differs from the given \a path.

//    Note that comparing paths may involve a per element comparison
//    which can be slow for complex paths.

//    \sa operator==()
//*/

//bool MyPainterPath::operator!=(const MyPainterPath &path) const
//{
//    return !(*this==path);
//}

///*!
//    \since 4.5

//    Returns the intersection of this path and the \a other path.

//    \sa intersected(), operator&=(), united(), operator|()
//*/
//MyPainterPath MyPainterPath::operator&(const MyPainterPath &other) const
//{
//    return intersected(other);
//}

///*!
//    \since 4.5

//    Returns the union of this path and the \a other path.

//    \sa united(), operator|=(), intersected(), operator&()
//*/
//MyPainterPath MyPainterPath::operator|(const MyPainterPath &other) const
//{
//    return united(other);
//}

///*!
//    \since 4.5

//    Returns the union of this path and the \a other path. This function is equivalent
//    to operator|().

//    \sa united(), operator+=(), operator-()
//*/
//MyPainterPath MyPainterPath::operator+(const MyPainterPath &other) const
//{
//    return united(other);
//}

///*!
//    \since 4.5

//    Subtracts the \a other path from a copy of this path, and returns the copy.

//    \sa subtracted(), operator-=(), operator+()
//*/
//MyPainterPath MyPainterPath::operator-(const MyPainterPath &other) const
//{
//    return subtracted(other);
//}

///*!
//    \since 4.5

//    Intersects this path with \a other and returns a reference to this path.

//    \sa intersected(), operator&(), operator|=()
//*/
//MyPainterPath &MyPainterPath::operator&=(const MyPainterPath &other)
//{
//    return *this = (*this & other);
//}

///*!
//    \since 4.5

//    Unites this path with \a other and returns a reference to this path.

//    \sa united(), operator|(), operator&=()
//*/
//MyPainterPath &MyPainterPath::operator|=(const MyPainterPath &other)
//{
//    return *this = (*this | other);
//}

///*!
//    \since 4.5

//    Unites this path with \a other, and returns a reference to this path. This
//    is equivalent to operator|=().

//    \sa united(), operator+(), operator-=()
//*/
//MyPainterPath &MyPainterPath::operator+=(const MyPainterPath &other)
//{
//    return *this = (*this + other);
//}

///*!
//    \since 4.5

//    Subtracts \a other from this path, and returns a reference to this
//    path.

//    \sa subtracted(), operator-(), operator+=()
//*/
//MyPainterPath &MyPainterPath::operator-=(const MyPainterPath &other)
//{
//    return *this = (*this - other);
//}

//#ifndef QT_NO_DATASTREAM
///*!
//    \fn QDataStream &operator<<(QDataStream &stream, const MyPainterPath &path)
//    \relates MyPainterPath

//    Writes the given painter \a path to the given \a stream, and
//    returns a reference to the \a stream.

//    \sa {Serializing Qt Data Types}
//*/
//QDataStream &operator<<(QDataStream &s, const MyPainterPath &p)
//{
//    if (p.isEmpty()) {
//        s << 0;
//        return s;
//    }

//    s << p.elementCount();
//    for (int i=0; i < p.d_func()->elements.size(); ++i) {
//        const MyPainterPath::Element &e = p.d_func()->elements.at(i);
//        s << int(e.type);
//        s << double(e.x) << double(e.y);
//    }
//    s << p.d_func()->cStart;
//    s << int(p.d_func()->fillRule);
//    return s;
//}

///*!
//    \fn QDataStream &operator>>(QDataStream &stream, MyPainterPath &path)
//    \relates MyPainterPath

//    Reads a painter path from the given \a stream into the specified \a path,
//    and returns a reference to the \a stream.

//    \sa {Serializing Qt Data Types}
//*/
//QDataStream &operator>>(QDataStream &s, MyPainterPath &p)
//{
//    bool errorDetected = false;
//    int size;
//    s >> size;

//    if (size == 0)
//        return s;

//    p.ensureData(); // in case if p.d_func() == 0
//    if (p.d_func()->elements.size() == 1) {
//        Q_ASSERT(p.d_func()->elements.at(0).type == MyPainterPath::MoveToElement);
//        p.d_func()->elements.clear();
//    }
//    for (int i=0; i<size; ++i) {
//        int type;
//        double x, y;
//        s >> type;
//        s >> x;
//        s >> y;
//        Q_ASSERT(type >= 0 && type <= 3);
//        if (!isValidCoord(qreal(x)) || !isValidCoord(qreal(y))) {
//#ifndef QT_NO_DEBUG
//            qWarning("QDataStream::operator>>: Invalid MyPainterPath coordinates read, skipping it");
//#endif
//            errorDetected = true;
//            continue;
//        }
//        MyPainterPath::Element elm = { qreal(x), qreal(y), MyPainterPath::ElementType(type) };
//        p.d_func()->elements.append(elm);
//    }
//    s >> p.d_func()->cStart;
//    int fillRule;
//    s >> fillRule;
//    Q_ASSERT(fillRule == Qt::OddEvenFill || fillRule == Qt::WindingFill);
//    p.d_func()->fillRule = Qt::FillRule(fillRule);
//    p.d_func()->dirtyBounds = true;
//    p.d_func()->dirtyControlBounds = true;
//    if (errorDetected)
//        p = MyPainterPath();  // Better than to return path with possibly corrupt datastructure, which would likely cause crash
//    return s;
//}
//#endif // QT_NO_DATASTREAM






















///*******************************************************************************
// * class MyPainterPathStroker -- IS IT NEEDED FOR OUR PURPOSES????? - TANMAY
// */

////void qt_path_stroke_move_to(qfixed x, qfixed y, void *data)
////{
////    ((MyPainterPath *) data)->moveTo(qt_fixed_to_real(x), qt_fixed_to_real(y));
////}

////void qt_path_stroke_line_to(qfixed x, qfixed y, void *data)
////{
////    ((MyPainterPath *) data)->lineTo(qt_fixed_to_real(x), qt_fixed_to_real(y));
////}

////void qt_path_stroke_cubic_to(qfixed c1x, qfixed c1y,
////                             qfixed c2x, qfixed c2y,
////                             qfixed ex, qfixed ey,
////                             void *data)
////{
////    ((MyPainterPath *) data)->cubicTo(qt_fixed_to_real(c1x), qt_fixed_to_real(c1y),
////                                     qt_fixed_to_real(c2x), qt_fixed_to_real(c2y),
////                                     qt_fixed_to_real(ex), qt_fixed_to_real(ey));
////}

/////*!
////    \since 4.1
////    \class MyPainterPathStroker
////    \ingroup painting
////    \inmodule QtGui

////    \brief The MyPainterPathStroker class is used to generate fillable
////    outlines for a given painter path.

////    By calling the createStroke() function, passing a given
////    MyPainterPath as argument, a new painter path representing the
////    outline of the given path is created. The newly created painter
////    path can then be filled to draw the original painter path's
////    outline.

////    You can control the various design aspects (width, cap styles,
////    join styles and dash pattern) of the outlining using the following
////    functions:

////    \list
////    \li setWidth()
////    \li setCapStyle()
////    \li setJoinStyle()
////    \li setDashPattern()
////    \endlist

////    The setDashPattern() function accepts both a Qt::PenStyle object
////    and a vector representation of the pattern as argument.

////    In addition you can specify a curve's threshold, controlling the
////    granularity with which a curve is drawn, using the
////    setCurveThreshold() function. The default threshold is a well
////    adjusted value (0.25), and normally you should not need to modify
////    it. However, you can make the curve's appearance smoother by
////    decreasing its value.

////    You can also control the miter limit for the generated outline
////    using the setMiterLimit() function. The miter limit describes how
////    far from each join the miter join can extend. The limit is
////    specified in the units of width so the pixelwise miter limit will
////    be \c {miterlimit * width}. This value is only used if the join
////    style is Qt::MiterJoin.

////    The painter path generated by the createStroke() function should
////    only be used for outlining the given painter path. Otherwise it
////    may cause unexpected behavior. Generated outlines also require the
////    Qt::WindingFill rule which is set by default.

////    \sa QPen, QBrush
////*/

////MyPainterPathStrokerPrivate::MyPainterPathStrokerPrivate()
////    : dashOffset(0)
////{
////    stroker.setMoveToHook(qt_path_stroke_move_to);
////    stroker.setLineToHook(qt_path_stroke_line_to);
////    stroker.setCubicToHook(qt_path_stroke_cubic_to);
////}

/////*!
////   Creates a new stroker.
//// */
////MyPainterPathStroker::MyPainterPathStroker()
////    : d_ptr(new MyPainterPathStrokerPrivate)
////{
////}

/////*!
////   Creates a new stroker based on \a pen.

////   \since 5.3
//// */
////MyPainterPathStroker::MyPainterPathStroker(const QPen &pen)
////    : d_ptr(new MyPainterPathStrokerPrivate)
////{
////    setWidth(pen.widthF());
////    setCapStyle(pen.capStyle());
////    setJoinStyle(pen.joinStyle());
////    setMiterLimit(pen.miterLimit());
////    setDashOffset(pen.dashOffset());

////    if (pen.style() == Qt::CustomDashLine)
////        setDashPattern(pen.dashPattern());
////    else
////        setDashPattern(pen.style());
////}

/////*!
////    Destroys the stroker.
////*/
////MyPainterPathStroker::~MyPainterPathStroker()
////{
////}


/////*!
////    Generates a new path that is a fillable area representing the
////    outline of the given \a path.

////    The various design aspects of the outline are based on the
////    stroker's properties: width(), capStyle(), joinStyle(),
////    dashPattern(), curveThreshold() and miterLimit().

////    The generated path should only be used for outlining the given
////    painter path. Otherwise it may cause unexpected
////    behavior. Generated outlines also require the Qt::WindingFill rule
////    which is set by default.
////*/
////MyPainterPath MyPainterPathStroker::createStroke(const MyPainterPath &path) const
////{
////    MyPainterPathStrokerPrivate *d = const_cast<MyPainterPathStrokerPrivate *>(d_func());
////    MyPainterPath stroke;
////    if (path.isEmpty())
////        return path;
////    if (d->dashPattern.isEmpty()) {
////        d->stroker.strokePath(path, &stroke, QTransform());
////    } else {
////        QDashStroker dashStroker(&d->stroker);
////        dashStroker.setDashPattern(d->dashPattern);
////        dashStroker.setDashOffset(d->dashOffset);
////        dashStroker.setClipRect(d->stroker.clipRect());
////        dashStroker.strokePath(path, &stroke, QTransform());
////    }
////    stroke.setFillRule(Qt::WindingFill);
////    return stroke;
////}

/////*!
////    Sets the width of the generated outline painter path to \a width.

////    The generated outlines will extend approximately 50% of \a width
////    to each side of the given input path's original outline.
////*/
////void MyPainterPathStroker::setWidth(qreal width)
////{
////    Q_D(MyPainterPathStroker);
////    if (width <= 0)
////        width = 1;
////    d->stroker.setStrokeWidth(qt_real_to_fixed(width));
////}

/////*!
////    Returns the width of the generated outlines.
////*/
////qreal MyPainterPathStroker::width() const
////{
////    return qt_fixed_to_real(d_func()->stroker.strokeWidth());
////}


/////*!
////    Sets the cap style of the generated outlines to \a style.  If a
////    dash pattern is set, each segment of the pattern is subject to the
////    cap \a style.
////*/
////void MyPainterPathStroker::setCapStyle(Qt::PenCapStyle style)
////{
////    d_func()->stroker.setCapStyle(style);
////}


/////*!
////    Returns the cap style of the generated outlines.
////*/
////Qt::PenCapStyle MyPainterPathStroker::capStyle() const
////{
////    return d_func()->stroker.capStyle();
////}

/////*!
////    Sets the join style of the generated outlines to \a style.
////*/
////void MyPainterPathStroker::setJoinStyle(Qt::PenJoinStyle style)
////{
////    d_func()->stroker.setJoinStyle(style);
////}

/////*!
////    Returns the join style of the generated outlines.
////*/
////Qt::PenJoinStyle MyPainterPathStroker::joinStyle() const
////{
////    return d_func()->stroker.joinStyle();
////}

/////*!
////    Sets the miter limit of the generated outlines to \a limit.

////    The miter limit describes how far from each join the miter join
////    can extend. The limit is specified in units of the currently set
////    width. So the pixelwise miter limit will be \c { miterlimit *
////    width}.

////    This value is only used if the join style is Qt::MiterJoin.
////*/
////void MyPainterPathStroker::setMiterLimit(qreal limit)
////{
////    d_func()->stroker.setMiterLimit(qt_real_to_fixed(limit));
////}

/////*!
////    Returns the miter limit for the generated outlines.
////*/
////qreal MyPainterPathStroker::miterLimit() const
////{
////    return qt_fixed_to_real(d_func()->stroker.miterLimit());
////}


/////*!
////    Specifies the curve flattening \a threshold, controlling the
////    granularity with which the generated outlines' curve is drawn.

////    The default threshold is a well adjusted value (0.25), and
////    normally you should not need to modify it. However, you can make
////    the curve's appearance smoother by decreasing its value.
////*/
////void MyPainterPathStroker::setCurveThreshold(qreal threshold)
////{
////    d_func()->stroker.setCurveThreshold(qt_real_to_fixed(threshold));
////}

/////*!
////    Returns the curve flattening threshold for the generated
////    outlines.
////*/
////qreal MyPainterPathStroker::curveThreshold() const
////{
////    return qt_fixed_to_real(d_func()->stroker.curveThreshold());
////}

/////*!
////    Sets the dash pattern for the generated outlines to \a style.
////*/
////void MyPainterPathStroker::setDashPattern(Qt::PenStyle style)
////{
////    d_func()->dashPattern = QDashStroker::patternForStyle(style);
////}

/////*!
////    \overload

////    Sets the dash pattern for the generated outlines to \a
////    dashPattern.  This function makes it possible to specify custom
////    dash patterns.

////    Each element in the vector contains the lengths of the dashes and spaces
////    in the stroke, beginning with the first dash in the first element, the
////    first space in the second element, and alternating between dashes and
////    spaces for each following pair of elements.

////    The vector can contain an odd number of elements, in which case the last
////    element will be extended by the length of the first element when the
////    pattern repeats.
////*/
////void MyPainterPathStroker::setDashPattern(const QVector<qreal> &dashPattern)
////{
////    d_func()->dashPattern.clear();
////    for (int i=0; i<dashPattern.size(); ++i)
////        d_func()->dashPattern << qt_real_to_fixed(dashPattern.at(i));
////}

























///*!
//    Returns the dash pattern for the generated outlines.
//*/











//// BELOW CODE CAUSED ERRORS -- TANMAY







////QVector<qreal> MyPainterPathStroker::dashPattern() const
////{
////    return d_func()->dashPattern;
////}

/////*!
////    Returns the dash offset for the generated outlines.
//// */
////qreal MyPainterPathStroker::dashOffset() const
////{
////    return d_func()->dashOffset;
////}

/////*!
////  Sets the dash offset for the generated outlines to \a offset.

////  See the documentation for QPen::setDashOffset() for a description of the
////  dash offset.
//// */
////void MyPainterPathStroker::setDashOffset(qreal offset)
////{
////    d_func()->dashOffset = offset;
////}

///*!
//  Converts the path into a polygon using the QTransform
//  \a matrix, and returns the polygon.

//  The polygon is created by first converting all subpaths to
//  polygons, then using a rewinding technique to make sure that
//  overlapping subpaths can be filled using the correct fill rule.

//  Note that rewinding inserts addition lines in the polygon so
//  the outline of the fill polygon does not match the outline of
//  the path.

//  \sa toSubpathPolygons(), toFillPolygons(),
//  {MyPainterPath#MyPainterPath Conversion}{MyPainterPath Conversion}
//*/
//QPolygonF MyPainterPath::toFillPolygon(const QTransform &matrix) const
//{

//    const QList<QPolygonF> flats = toSubpathPolygons(matrix);
//    QPolygonF polygon;
//    if (flats.isEmpty())
//        return polygon;
//    QPointF first = flats.first().first();
//    for (int i=0; i<flats.size(); ++i) {
//        polygon += flats.at(i);
//        if (!flats.at(i).isClosed())
//            polygon += flats.at(i).first();
//        if (i > 0)
//            polygon += first;
//    }
//    return polygon;
//}

//#if QT_DEPRECATED_SINCE(5, 15)
///*!
//  \overload
//  \obsolete

//  Use toFillPolygon(const QTransform &matrix) instead.
//*/
//QPolygonF MyPainterPath::toFillPolygon(const QMatrix &matrix) const
//{
//    return toFillPolygon(QTransform(matrix));
//}
//#endif // QT_DEPRECATED_SINCE(5, 15)

////derivative of the equation
//static inline qreal slopeAt(qreal t, qreal a, qreal b, qreal c, qreal d)
//{
//    return 3*t*t*(d - 3*c + 3*b - a) + 6*t*(c - 2*b + a) + 3*(b - a);
//}

///*!
//    Returns the length of the current path.
//*/
//qreal MyPainterPath::length() const
//{
//    Q_D(MyPainterPath);
//    if (isEmpty())
//        return 0;

//    qreal len = 0;
//    for (int i=1; i<d->elements.size(); ++i) {
//        const Element &e = d->elements.at(i);

//        switch (e.type) {
//        case MoveToElement:
//            break;
//        case LineToElement:
//        {
//            len += QLineF(d->elements.at(i-1), e).length();
//            break;
//        }
//        case CurveToElement:
//        {
//            QBezier b = QBezier::fromPoints(d->elements.at(i-1),
//                                            e,
//                                            d->elements.at(i+1),
//                                            d->elements.at(i+2));
//            len += b.length();
//            i += 2;
//            break;
//        }
//        default:
//            break;
//        }
//    }
//    return len;
//}

///*!
//    Returns percentage of the whole path at the specified length \a len.

//    Note that similarly to other percent methods, the percentage measurement
//    is not linear with regards to the length, if curves are present
//    in the path. When curves are present the percentage argument is mapped
//    to the t parameter of the Bezier equations.
//*/
//qreal MyPainterPath::percentAtLength(qreal len) const
//{
//    Q_D(MyPainterPath);
//    if (isEmpty() || len <= 0)
//        return 0;

//    qreal totalLength = length();
//    if (len > totalLength)
//        return 1;

//    qreal curLen = 0;
//    for (int i=1; i<d->elements.size(); ++i) {
//        const Element &e = d->elements.at(i);

//        switch (e.type) {
//        case MoveToElement:
//            break;
//        case LineToElement:
//        {
//            QLineF line(d->elements.at(i-1), e);
//            qreal llen = line.length();
//            curLen += llen;
//            if (curLen >= len) {
//                return len/totalLength ;
//            }

//            break;
//        }
//        case CurveToElement:
//        {
//            QBezier b = QBezier::fromPoints(d->elements.at(i-1),
//                                            e,
//                                            d->elements.at(i+1),
//                                            d->elements.at(i+2));
//            qreal blen = b.length();
//            qreal prevLen = curLen;
//            curLen += blen;

//            if (curLen >= len) {
//                qreal res = b.tAtLength(len - prevLen);
//                return (res * blen + prevLen)/totalLength;
//            }

//            i += 2;
//            break;
//        }
//        default:
//            break;
//        }
//    }

//    return 0;
//}

//static inline QBezier bezierAtT(const MyPainterPath &path, qreal t, qreal *startingLength, qreal *bezierLength)
//{
//    *startingLength = 0;
//    if (t > 1)
//        return QBezier();

//    qreal curLen = 0;
//    qreal totalLength = path.length();

//    const int lastElement = path.elementCount() - 1;
//    for (int i=0; i <= lastElement; ++i) {
//        const MyPainterPath::Element &e = path.elementAt(i);

//        switch (e.type) {
//        case MyPainterPath::MoveToElement:
//            break;
//        case MyPainterPath::LineToElement:
//        {
//            QLineF line(path.elementAt(i-1), e);
//            qreal llen = line.length();
//            curLen += llen;
//            if (i == lastElement || curLen/totalLength >= t) {
//                *bezierLength = llen;
//                QPointF a = path.elementAt(i-1);
//                QPointF delta = e - a;
//                return QBezier::fromPoints(a, a + delta / 3, a + 2 * delta / 3, e);
//            }
//            break;
//        }
//        case MyPainterPath::CurveToElement:
//        {
//            QBezier b = QBezier::fromPoints(path.elementAt(i-1),
//                                            e,
//                                            path.elementAt(i+1),
//                                            path.elementAt(i+2));
//            qreal blen = b.length();
//            curLen += blen;

//            if (i + 2 == lastElement || curLen/totalLength >= t) {
//                *bezierLength = blen;
//                return b;
//            }

//            i += 2;
//            break;
//        }
//        default:
//            break;
//        }
//        *startingLength = curLen;
//    }
//    return QBezier();
//}

///*!
//    Returns the point at at the percentage \a t of the current path.
//    The argument \a t has to be between 0 and 1.

//    Note that similarly to other percent methods, the percentage measurement
//    is not linear with regards to the length, if curves are present
//    in the path. When curves are present the percentage argument is mapped
//    to the t parameter of the Bezier equations.
//*/
//QPointF MyPainterPath::pointAtPercent(qreal t) const
//{
//    if (t < 0 || t > 1) {
//        qWarning("MyPainterPath::pointAtPercent accepts only values between 0 and 1");
//        return QPointF();
//    }

//    if (!d_ptr || d_ptr->elements.size() == 0)
//        return QPointF();

//    if (d_ptr->elements.size() == 1)
//        return d_ptr->elements.at(0);

//    qreal totalLength = length();
//    qreal curLen = 0;
//    qreal bezierLen = 0;
//    QBezier b = bezierAtT(*this, t, &curLen, &bezierLen);
//    qreal realT = (totalLength * t - curLen) / bezierLen;

//    return b.pointAt(qBound(qreal(0), realT, qreal(1)));
//}

///*!
//    Returns the angle of the path tangent at the percentage \a t.
//    The argument \a t has to be between 0 and 1.

//    Positive values for the angles mean counter-clockwise while negative values
//    mean the clockwise direction. Zero degrees is at the 3 o'clock position.

//    Note that similarly to the other percent methods, the percentage measurement
//    is not linear with regards to the length if curves are present
//    in the path. When curves are present the percentage argument is mapped
//    to the t parameter of the Bezier equations.
//*/
//qreal MyPainterPath::angleAtPercent(qreal t) const
//{
//    if (t < 0 || t > 1) {
//        qWarning("MyPainterPath::angleAtPercent accepts only values between 0 and 1");
//        return 0;
//    }

//    qreal totalLength = length();
//    qreal curLen = 0;
//    qreal bezierLen = 0;
//    QBezier bez = bezierAtT(*this, t, &curLen, &bezierLen);
//    qreal realT = (totalLength * t - curLen) / bezierLen;

//    qreal m1 = slopeAt(realT, bez.x1, bez.x2, bez.x3, bez.x4);
//    qreal m2 = slopeAt(realT, bez.y1, bez.y2, bez.y3, bez.y4);

//    return QLineF(0, 0, m1, m2).angle();
//}


///*!
//    Returns the slope of the path at the percentage \a t. The
//    argument \a t has to be between 0 and 1.

//    Note that similarly to other percent methods, the percentage measurement
//    is not linear with regards to the length, if curves are present
//    in the path. When curves are present the percentage argument is mapped
//    to the t parameter of the Bezier equations.
//*/
//qreal MyPainterPath::slopeAtPercent(qreal t) const
//{
//    if (t < 0 || t > 1) {
//        qWarning("MyPainterPath::slopeAtPercent accepts only values between 0 and 1");
//        return 0;
//    }

//    qreal totalLength = length();
//    qreal curLen = 0;
//    qreal bezierLen = 0;
//    QBezier bez = bezierAtT(*this, t, &curLen, &bezierLen);
//    qreal realT = (totalLength * t - curLen) / bezierLen;

//    qreal m1 = slopeAt(realT, bez.x1, bez.x2, bez.x3, bez.x4);
//    qreal m2 = slopeAt(realT, bez.y1, bez.y2, bez.y3, bez.y4);
//    //tangent line
//    qreal slope = 0;

//    if (m1)
//        slope = m2/m1;
//    else {
//        if (std::numeric_limits<qreal>::has_infinity) {
//            slope = (m2  < 0) ? -std::numeric_limits<qreal>::infinity()
//                              : std::numeric_limits<qreal>::infinity();
//        } else {
//            if (sizeof(qreal) == sizeof(double)) {
//                return 1.79769313486231570e+308;
//            } else {
//                return ((qreal)3.40282346638528860e+38);
//            }
//        }
//    }

//    return slope;
//}

///*!
//  \since 4.4

//  Adds the given rectangle \a rect with rounded corners to the path.

//  The \a xRadius and \a yRadius arguments specify the radii of
//  the ellipses defining the corners of the rounded rectangle.
//  When \a mode is Qt::RelativeSize, \a xRadius and
//  \a yRadius are specified in percentage of half the rectangle's
//  width and height respectively, and should be in the range 0.0 to 100.0.

//  \sa addRect()
//*/
//void MyPainterPath::addRoundedRect(const QRectF &rect, qreal xRadius, qreal yRadius,
//                                  Qt::SizeMode mode)
//{
//    QRectF r = rect.normalized();

//    if (r.isNull())
//        return;

//    if (mode == Qt::AbsoluteSize) {
//        qreal w = r.width() / 2;
//        qreal h = r.height() / 2;

//        if (w == 0) {
//            xRadius = 0;
//        } else {
//            xRadius = 100 * qMin(xRadius, w) / w;
//        }
//        if (h == 0) {
//            yRadius = 0;
//        } else {
//            yRadius = 100 * qMin(yRadius, h) / h;
//        }
//    } else {
//        if (xRadius > 100)                          // fix ranges
//            xRadius = 100;

//        if (yRadius > 100)
//            yRadius = 100;
//    }

//    if (xRadius <= 0 || yRadius <= 0) {             // add normal rectangle
//        addRect(r);
//        return;
//    }

//    qreal x = r.x();
//    qreal y = r.y();
//    qreal w = r.width();
//    qreal h = r.height();
//    qreal rxx2 = w*xRadius/100;
//    qreal ryy2 = h*yRadius/100;

//    ensureData();
//    detach();

//    bool first = d_func()->elements.size() < 2;

//    arcMoveTo(x, y, rxx2, ryy2, 180);
//    arcTo(x, y, rxx2, ryy2, 180, -90);
//    arcTo(x+w-rxx2, y, rxx2, ryy2, 90, -90);
//    arcTo(x+w-rxx2, y+h-ryy2, rxx2, ryy2, 0, -90);
//    arcTo(x, y+h-ryy2, rxx2, ryy2, 270, -90);
//    closeSubpath();

//    d_func()->require_moveTo = true;
//    d_func()->convex = first;
//}

///*!
//  \fn void MyPainterPath::addRoundedRect(qreal x, qreal y, qreal w, qreal h, qreal xRadius, qreal yRadius, Qt::SizeMode mode = Qt::AbsoluteSize);
//  \since 4.4
//  \overload

//  Adds the given rectangle \a x, \a y, \a w, \a h  with rounded corners to the path.
// */

//#if QT_DEPRECATED_SINCE(5, 13)
///*!
//  \obsolete

//  Adds a rectangle \a r with rounded corners to the path.

//  The \a xRnd and \a yRnd arguments specify how rounded the corners
//  should be. 0 is angled corners, 99 is maximum roundedness.

//  \sa addRoundedRect()
//*/
//void MyPainterPath::addRoundRect(const QRectF &r, int xRnd, int yRnd)
//{
//    if(xRnd >= 100)                          // fix ranges
//        xRnd = 99;
//    if(yRnd >= 100)
//        yRnd = 99;
//    if(xRnd <= 0 || yRnd <= 0) {             // add normal rectangle
//        addRect(r);
//        return;
//    }

//    QRectF rect = r.normalized();

//    if (rect.isNull())
//        return;

//    qreal x = rect.x();
//    qreal y = rect.y();
//    qreal w = rect.width();
//    qreal h = rect.height();
//    qreal rxx2 = w*xRnd/100;
//    qreal ryy2 = h*yRnd/100;

//    ensureData();
//    detach();

//    bool first = d_func()->elements.size() < 2;

//    arcMoveTo(x, y, rxx2, ryy2, 180);
//    arcTo(x, y, rxx2, ryy2, 180, -90);
//    arcTo(x+w-rxx2, y, rxx2, ryy2, 90, -90);
//    arcTo(x+w-rxx2, y+h-ryy2, rxx2, ryy2, 0, -90);
//    arcTo(x, y+h-ryy2, rxx2, ryy2, 270, -90);
//    closeSubpath();

//    d_func()->require_moveTo = true;
//    d_func()->convex = first;
//}

///*!
//  \obsolete

//  \fn bool MyPainterPath::addRoundRect(const QRectF &rect, int roundness);
//  \since 4.3
//  \overload

//  Adds a rounded rectangle, \a rect, to the path.

//  The \a roundness argument specifies uniform roundness for the
//  rectangle.  Vertical and horizontal roundness factors will be
//  adjusted accordingly to act uniformly around both axes. Use this
//  method if you want a rectangle equally rounded across both the X and
//  Y axis.

//  \sa addRoundedRect()
//*/
//void MyPainterPath::addRoundRect(const QRectF &rect,
//                                int roundness)
//{
//    int xRnd = roundness;
//    int yRnd = roundness;
//    if (rect.width() > rect.height())
//        xRnd = int(roundness * rect.height()/rect.width());
//    else
//        yRnd = int(roundness * rect.width()/rect.height());
//    addRoundedRect(rect, xRnd, yRnd, Qt::RelativeSize);
//}

///*!
//  \obsolete

//  \fn void MyPainterPath::addRoundRect(qreal x, qreal y, qreal w, qreal h, int xRnd, int yRnd);
//  \overload

//  Adds a rectangle with rounded corners to the path. The rectangle
//  is constructed from \a x, \a y, and the width and height \a w
//  and \a h.

//  The \a xRnd and \a yRnd arguments specify how rounded the corners
//  should be. 0 is angled corners, 99 is maximum roundedness.

//  \sa addRoundedRect()
// */
//void MyPainterPath::addRoundRect(qreal x, qreal y, qreal w, qreal h,
//                                int xRnd, int yRnd)
//{
//    addRoundedRect(QRectF(x, y, w, h), xRnd, yRnd, Qt::RelativeSize);
//}

///*!
//  \obsolete

//  \fn bool MyPainterPath::addRoundRect(qreal x, qreal y, qreal width, qreal height, int roundness);
//  \since 4.3
//  \overload

//  Adds a rounded rectangle to the path, defined by the coordinates \a
//  x and \a y with the specified \a width and \a height.

//  The \a roundness argument specifies uniform roundness for the
//  rectangle. Vertical and horizontal roundness factors will be
//  adjusted accordingly to act uniformly around both axes. Use this
//  method if you want a rectangle equally rounded across both the X and
//  Y axis.

//  \sa addRoundedRect()
//*/
//void MyPainterPath::addRoundRect(qreal x, qreal y, qreal w, qreal h,
//                                int roundness)
//{
//    addRoundedRect(QRectF(x, y, w, h), roundness, Qt::RelativeSize);
//}
//#endif

///*!
//    \since 4.3

//    Returns a path which is the union of this path's fill area and \a p's fill area.

//    Set operations on paths will treat the paths as areas. Non-closed
//    paths will be treated as implicitly closed.
//    Bezier curves may be flattened to line segments due to numerical instability of
//    doing bezier curve intersections.

//    \sa intersected(), subtracted()
//*/
//MyPainterPath MyPainterPath::united(const MyPainterPath &p) const
//{
//    if (isEmpty() || p.isEmpty())
//        return isEmpty() ? p : *this;
//    KisPathClipper clipper(*this, p);
//    return clipper.clip(KisPathClipper::BoolOr);
//}

///*!
//    \since 4.3

//    Returns a path which is the intersection of this path's fill area and \a p's fill area.
//    Bezier curves may be flattened to line segments due to numerical instability of
//    doing bezier curve intersections.
//*/
//MyPainterPath MyPainterPath::intersected(const MyPainterPath &p) const
//{
//    if (isEmpty() || p.isEmpty())
//        return MyPainterPath();
//    KisPathClipper clipper(*this, p);
//    return clipper.clip(KisPathClipper::BoolAnd);
//}

///*!
//    \since 4.3

//    Returns a path which is \a p's fill area subtracted from this path's fill area.

//    Set operations on paths will treat the paths as areas. Non-closed
//    paths will be treated as implicitly closed.
//    Bezier curves may be flattened to line segments due to numerical instability of
//    doing bezier curve intersections.
//*/
//MyPainterPath MyPainterPath::subtracted(const MyPainterPath &p) const
//{
//    if (isEmpty() || p.isEmpty())
//        return *this;
//    KisPathClipper clipper(*this, p);
//    return clipper.clip(KisPathClipper::BoolSub);
//}

//#if QT_DEPRECATED_SINCE(5, 13)
///*!
//    \since 4.3
//    \obsolete

//    Use subtracted() instead.

//    \sa subtracted()
//*/
//MyPainterPath MyPainterPath::subtractedInverted(const MyPainterPath &p) const
//{
//    return p.subtracted(*this);
//}
//#endif

///*!
//    \since 4.4

//    Returns a simplified version of this path. This implies merging all subpaths that intersect,
//    and returning a path containing no intersecting edges. Consecutive parallel lines will also
//    be merged. The simplified path will always use the default fill rule, Qt::OddEvenFill.
//    Bezier curves may be flattened to line segments due to numerical instability of
//    doing bezier curve intersections.
//*/
//MyPainterPath MyPainterPath::simplified() const
//{
//    if(isEmpty())
//        return *this;
//    KisPathClipper clipper(*this, MyPainterPath());
//    return clipper.clip(KisPathClipper::Simplify);
//}

///*!
//  \since 4.3

//  Returns \c true if the current path intersects at any point the given path \a p.
//  Also returns \c true if the current path contains or is contained by any part of \a p.

//  Set operations on paths will treat the paths as areas. Non-closed
//  paths will be treated as implicitly closed.

//  \sa contains()
// */
//bool MyPainterPath::intersects(const MyPainterPath &p) const
//{
//    if (p.elementCount() == 1)
//        return contains(p.elementAt(0));
//    if (isEmpty() || p.isEmpty())
//        return false;
//    KisPathClipper clipper(*this, p);
//    return clipper.intersect();
//}

///*!
//  \since 4.3

//  Returns \c true if the given path \a p is contained within
//  the current path. Returns \c false if any edges of the current path and
//  \a p intersect.

//  Set operations on paths will treat the paths as areas. Non-closed
//  paths will be treated as implicitly closed.

//  \sa intersects()
// */
//bool MyPainterPath::contains(const MyPainterPath &p) const
//{
//    if (p.elementCount() == 1)
//        return contains(p.elementAt(0));
//    if (isEmpty() || p.isEmpty())
//        return false;
//    KisPathClipper clipper(*this, p);
//    return clipper.contains();
//}

//void MyPainterPath::setDirty(bool dirty)
//{
//    d_func()->dirtyBounds        = dirty;
//    d_func()->dirtyControlBounds = dirty;
//    d_func()->pathConverter.reset();
//    d_func()->convex = false;
//}

//void MyPainterPath::computeBoundingRect() const
//{
//    MyPainterPathData *d = d_func();
//    d->dirtyBounds = false;
//    if (!d_ptr) {
//        d->bounds = QRect();
//        return;
//    }

//    qreal minx, maxx, miny, maxy;
//    minx = maxx = d->elements.at(0).x;
//    miny = maxy = d->elements.at(0).y;
//    for (int i=1; i<d->elements.size(); ++i) {
//        const Element &e = d->elements.at(i);

//        switch (e.type) {
//        case MoveToElement:
//        case LineToElement:
//            if (e.x > maxx) maxx = e.x;
//            else if (e.x < minx) minx = e.x;
//            if (e.y > maxy) maxy = e.y;
//            else if (e.y < miny) miny = e.y;
//            break;
//        case CurveToElement:
//            {
//                QBezier b = QBezier::fromPoints(d->elements.at(i-1),
//                                                e,
//                                                d->elements.at(i+1),
//                                                d->elements.at(i+2));
//                QRectF r = qt_painterpath_bezier_extrema(b);
//                qreal right = r.right();
//                qreal bottom = r.bottom();
//                if (r.x() < minx) minx = r.x();
//                if (right > maxx) maxx = right;
//                if (r.y() < miny) miny = r.y();
//                if (bottom > maxy) maxy = bottom;
//                i += 2;
//            }
//            break;
//        default:
//            break;
//        }
//    }
//    d->bounds = QRectF(minx, miny, maxx - minx, maxy - miny);
//}


//void MyPainterPath::computeControlPointRect() const
//{
//    MyPainterPathData *d = d_func();
//    d->dirtyControlBounds = false;
//    if (!d_ptr) {
//        d->controlBounds = QRect();
//        return;
//    }

//    qreal minx, maxx, miny, maxy;
//    minx = maxx = d->elements.at(0).x;
//    miny = maxy = d->elements.at(0).y;
//    for (int i=1; i<d->elements.size(); ++i) {
//        const Element &e = d->elements.at(i);
//        if (e.x > maxx) maxx = e.x;
//        else if (e.x < minx) minx = e.x;
//        if (e.y > maxy) maxy = e.y;
//        else if (e.y < miny) miny = e.y;
//    }
//    d->controlBounds = QRectF(minx, miny, maxx - minx, maxy - miny);
//}

//#ifndef QT_NO_DEBUG_STREAM

//QDebug operator<<(QDebug s, const MyPainterPath &p)
//{
//    s.nospace() << "MyPainterPath: Element count=" << p.elementCount() << '\n';
//    const char *types[] = {"MoveTo", "LineTo", "CurveTo", "CurveToData"};
//    for (int i=0; i<p.elementCount(); ++i) {
//        s.nospace() << " -> " << types[p.elementAt(i).type] << "(x=" << p.elementAt(i).x << ", y=" << p.elementAt(i).y << ')' << '\n';

//    }
//    return s;
//}
//#endif

//QT_END_NAMESPACE
