//#ifndef KISBOOLEANOPERATIONS_H
//#define KISBOOLEANOPERATIONS_H

//#include <kisintersectionfinder.h>
//#include <painterpath.h>
//#include <painterpath_p.h>
//#include <pathclipper.h>
//#include <QPainterPath>

//enum ClipperMode {
//    ClipMode, // do the full clip
//    CheckMode // for contains/intersects (only interested in whether the result path is non-empty)
//};

//struct QCrossingEdge
//{
//    int edge;
//    qreal x;

//    bool operator<(const QCrossingEdge &edge) const
//    {
//        return x < edge.x;
//    }
//};

//class KEdge
//{
//public:

//    KEdge();
//    KEdge(BuildingBlock element);

//    qreal top;
//    qreal bottom;

////private:

//    BuildingBlock element;
//    int flag;

//    enum Traversal {
//        RightTraversal,
//        LeftTraversal
//    };

//    enum Direction {
//        Forward,
//        Backward
//    };

//    int windingA;
//    int windingB;

//    enum edgeType{
//        line,
//        curve
//    };


//    qreal left;
//    qreal right;

//    edgeType type;

//};

//class KisBooleanOperations
//{
//public:
//    enum Operation {
//        BoolAnd,
//        BoolOr,
//        BoolSub,
//        Simplify
//    };

//    Operation op;
//    KisBooleanOperations();
//    KisBooleanOperations( KisIntersectionFinder kif, Operation op);

//    QPainterPath clip( KisIntersectionFinder kif, ClipperMode mode );

//    QVector<QCrossingEdge> findCrossings( qreal y);
//    bool handleCrossingEdges( qreal y, ClipperMode mode);



//    int aMask;
//    int bMask;

//    struct TraversalStatus
//    {
//        int edge;
//        KEdge::Traversal traversal;
//        KEdge::Direction direction;

//        void flipDirection();
//        void flipTraversal();

//        void flip();
//    };

//    TraversalStatus next(const KisBooleanOperations::TraversalStatus &status) const;



////private:
//    QVector<QPointF> vertices;
//    QVector<KEdge> segments;
    
    
//};

//#endif // KISBOOLEANOPERATIONS_H

/*
 *  SPDX-FileCopyrightText: 2021 Tanmay Chavan <earendil01tc@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef KISBOOLEANOPERATIONS_H
#define KISBOOLEANOPERATIONS_H

#include "kisintersectionfinder.h"
//#include "painterpath.h"
//#include "painterpath_p.h"
#include "kispathclipper.h"
#include <QPainterPath>

//enum ClipperMode {
//    ClipMode, // do the full clip
//    CheckMode // for contains/intersects (only interested in whether the result path is non-empty)
//};

class KisBooleanOperations {

public:

    KisBooleanOperations();
    ~KisBooleanOperations();

    QPainterPath toMyPainterPath(const QPainterPath &path);

    QPainterPath uniteAndAdd(QPainterPath &sub, QPainterPath &clip);

    QPainterPath intersectAndAdd(const QPainterPath &sub, const QPainterPath &clip);
    QPainterPath subtractAndAdd(const QPainterPath &sub, const QPainterPath &clip);

    void addSubjectPathList();
    void addClipPathList();

    QPainterPath testAdd();
};

#endif
